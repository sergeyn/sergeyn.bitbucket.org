var ca = "function" == typeof Object.defineProperties ? Object.defineProperty : function(a, b, d) {
        if (d.get || d.set) throw new TypeError("ES3 does not support getters and setters.");
        a != Array.prototype && a != Object.prototype && (a[b] = d.value)
    },
    k = "undefined" != typeof window && window === this ? this : "undefined" != typeof global && null != global ? global : this;

function n() {
    n = function() {};
    k.Symbol || (k.Symbol = da)
}
var ea = 0;

function da(a) {
    return "jscomp_symbol_" + (a || "") + ea++
}

function q() {
    n();
    var a = k.Symbol.iterator;
    a || (a = k.Symbol.iterator = k.Symbol("iterator"));
    "function" != typeof Array.prototype[a] && ca(Array.prototype, a, {
        configurable: !0,
        writable: !0,
        value: function() {
            return fa(this)
        }
    });
    q = function() {}
}

function fa(a) {
    var b = 0;
    return ga(function() {
        return b < a.length ? {
            done: !1,
            value: a[b++]
        } : {
            done: !0
        }
    })
}

function ga(a) {
    q();
    a = {
        next: a
    };
    a[k.Symbol.iterator] = function() {
        return this
    };
    return a
}

function r(a) {
    q();
    var b = a[Symbol.iterator];
    return b ? b.call(a) : fa(a)
}

function v(a, b) {
    return Object.prototype.hasOwnProperty.call(a, b)
}

function ha(a, b) {
    if (b) {
        for (var d = k, e = a.split("."), c = 0; c < e.length - 1; c++) {
            var f = e[c];
            f in d || (d[f] = {});
            d = d[f]
        }
        e = e[e.length - 1];
        c = d[e];
        f = b(c);
        f != c && null != f && ca(d, e, {
            configurable: !0,
            writable: !0,
            value: f
        })
    }
}
ha("WeakMap", function(a) {
    function b(a) {
        this.w = (f += Math.random() + 1).toString();
        if (a) {
            n();
            q();
            a = r(a);
            for (var b; !(b = a.next()).done;) b = b.value, this.set(b[0], b[1])
        }
    }

    function d(a) {
        v(a, c) || ca(a, c, {
            value: {}
        })
    }

    function e(a) {
        var b = Object[a];
        b && (Object[a] = function(a) {
            d(a);
            return b(a)
        })
    }
    if (function() {
            if (!a || !Object.seal) return !1;
            try {
                var b = Object.seal({}),
                    c = Object.seal({}),
                    d = new a([
                        [b, 2],
                        [c, 3]
                    ]);
                if (2 != d.get(b) || 3 != d.get(c)) return !1;
                d["delete"](b);
                d.set(c, 4);
                return !d.has(b) && 4 == d.get(c)
            } catch (m) {
                return !1
            }
        }()) return a;
    var c = "$jscomp_hidden_" + Math.random().toString().substring(2);
    e("freeze");
    e("preventExtensions");
    e("seal");
    var f = 0;
    b.prototype.set = function(a, b) {
        d(a);
        if (!v(a, c)) throw Error("WeakMap key fail: " + a);
        a[c][this.w] = b;
        return this
    };
    b.prototype.get = function(a) {
        return v(a, c) ? a[c][this.w] : void 0
    };
    b.prototype.has = function(a) {
        return v(a, c) && v(a[c], this.w)
    };
    b.prototype["delete"] = function(a) {
        return v(a, c) && v(a[c], this.w) ? delete a[c][this.w] : !1
    };
    return b
});
ha("Map", function(a) {
    function b() {
        var a = {};
        return a.o = a.next = a.head = a
    }

    function d(a, b) {
        var c = a.m;
        return ga(function() {
            if (c) {
                for (; c.head != a.m;) c = c.o;
                for (; c.next != c.head;) return c = c.next, {
                    done: !1,
                    value: b(c)
                };
                c = null
            }
            return {
                done: !0,
                value: void 0
            }
        })
    }

    function e(a, b) {
        var c;
        c = b && typeof b;
        "object" == c || "function" == c ? f.has(b) ? c = f.get(b) : (c = "" + ++g, f.set(b, c)) : c = "p_" + b;
        var d = a.v[c];
        if (d && v(a.v, c))
            for (var e = 0; e < d.length; e++) {
                var h = d[e];
                if (b !== b && h.key !== h.key || b === h.key) return {
                    id: c,
                    list: d,
                    index: e,
                    h: h
                }
            }
        return {
            id: c,
            list: d,
            index: -1,
            h: void 0
        }
    }

    function c(a) {
        this.v = {};
        this.m = b();
        this.size = 0;
        if (a) {
            a = r(a);
            for (var c; !(c = a.next()).done;) c = c.value, this.set(c[0], c[1])
        }
    }
    if (function() {
            if (!a || !a.prototype.entries || "function" != typeof Object.seal) return !1;
            try {
                var b = Object.seal({
                        x: 4
                    }),
                    c = new a(r([
                        [b, "s"]
                    ]));
                if ("s" != c.get(b) || 1 != c.size || c.get({
                        x: 4
                    }) || c.set({
                        x: 4
                    }, "t") != c || 2 != c.size) return !1;
                var d = c.entries(),
                    e = d.next();
                if (e.done || e.value[0] != b || "s" != e.value[1]) return !1;
                e = d.next();
                return e.done || 4 != e.value[0].x || "t" != e.value[1] ||
                    !d.next().done ? !1 : !0
            } catch (t) {
                return !1
            }
        }()) return a;
    n();
    q();
    var f = new WeakMap;
    c.prototype.set = function(a, b) {
        var c = e(this, a);
        c.list || (c.list = this.v[c.id] = []);
        c.h ? c.h.value = b : (c.h = {
            next: this.m,
            o: this.m.o,
            head: this.m,
            key: a,
            value: b
        }, c.list.push(c.h), this.m.o.next = c.h, this.m.o = c.h, this.size++);
        return this
    };
    c.prototype["delete"] = function(a) {
        a = e(this, a);
        return a.h && a.list ? (a.list.splice(a.index, 1), a.list.length || delete this.v[a.id], a.h.o.next = a.h.next, a.h.next.o = a.h.o, a.h.head = null, this.size--, !0) : !1
    };
    c.prototype.clear =
        function() {
            this.v = {};
            this.m = this.m.o = b();
            this.size = 0
        };
    c.prototype.has = function(a) {
        return !!e(this, a).h
    };
    c.prototype.get = function(a) {
        return (a = e(this, a).h) && a.value
    };
    c.prototype.entries = function() {
        return d(this, function(a) {
            return [a.key, a.value]
        })
    };
    c.prototype.keys = function() {
        return d(this, function(a) {
            return a.key
        })
    };
    c.prototype.values = function() {
        return d(this, function(a) {
            return a.value
        })
    };
    c.prototype.forEach = function(a, b) {
        for (var c = this.entries(), d; !(d = c.next()).done;) d = d.value, a.call(b, d[1], d[0], this)
    };
    c.prototype[Symbol.iterator] = c.prototype.entries;
    var g = 0;
    return c
});
ha("Set", function(a) {
    function b(a) {
        this.j = new Map;
        if (a) {
            a = r(a);
            for (var b; !(b = a.next()).done;) this.add(b.value)
        }
        this.size = this.j.size
    }
    if (function() {
            if (!a || !a.prototype.entries || "function" != typeof Object.seal) return !1;
            try {
                var b = Object.seal({
                        x: 4
                    }),
                    e = new a(r([b]));
                if (!e.has(b) || 1 != e.size || e.add(b) != e || 1 != e.size || e.add({
                        x: 4
                    }) != e || 2 != e.size) return !1;
                var c = e.entries(),
                    f = c.next();
                if (f.done || f.value[0] != b || f.value[1] != b) return !1;
                f = c.next();
                return f.done || f.value[0] == b || 4 != f.value[0].x || f.value[1] != f.value[0] ?
                    !1 : c.next().done
            } catch (g) {
                return !1
            }
        }()) return a;
    n();
    q();
    b.prototype.add = function(a) {
        this.j.set(a, a);
        this.size = this.j.size;
        return this
    };
    b.prototype["delete"] = function(a) {
        a = this.j["delete"](a);
        this.size = this.j.size;
        return a
    };
    b.prototype.clear = function() {
        this.j.clear();
        this.size = 0
    };
    b.prototype.has = function(a) {
        return this.j.has(a)
    };
    b.prototype.entries = function() {
        return this.j.entries()
    };
    b.prototype.values = function() {
        return this.j.values()
    };
    b.prototype[Symbol.iterator] = b.prototype.values;
    b.prototype.forEach =
        function(a, b) {
            var c = this;
            this.j.forEach(function(d) {
                return a.call(b, d, d, c)
            })
        };
    return b
});
ha("Array.prototype.fill", function(a) {
    return a ? a : function(a, d, e) {
        var b = this.length || 0;
        0 > d && (d = Math.max(0, b + d));
        if (null == e || e > b) e = b;
        e = Number(e);
        0 > e && (e = Math.max(0, b + e));
        for (d = Number(d || 0); d < e; d++) this[d] = a;
        return this
    }
});
var ia = [
        ["Onax", "Zbahzrag"],
        ["Rqtjner Ebnq", "Rqtjner Ebnq S"],
        ["Cnqqvatgba", "Cnqqvatgba S"],
        ["Pyncunz Abegu", "Pyncunz Uvtu Fgerrg"],
        ["Unpxarl Qbjaf", "Unpxarl Prageny"],
        ["Rzvengrf Terrajvpu Cravafhyn", "Abegu Terrajvpu"],
        ["Rzvengrf Eblny Qbpxf", "Eblny Ivpgbevn"],
        ["Gbjre Uvyy", "Gbjre Tngrjnl"]
    ],
    ja = [
        ["Obj Puhepu", "Obj Ebnq"],
        ["Oebaqrfohel", "Xvyohea"],
        ["Pnzqra Ebnq", "Pnzqra Gbja"],
        ["Pnanel Junes", "Ureba Dhnlf"],
        ["Qnyfgba Whapgvba", "Qnyfgba Xvatfynaq"],
        ["Rhfgba", "Rhfgba Square"],
        ["Svapuyrl Ebnq", "Svapuyrl Ebnq & Frognal"],
        ["Unatre Ynar", "Cnex Eblny"],
        ["Vpxraunz", "Jrfg Ehvfyvc"],
        ["Xragvfu Gbja", "Xragvfu Gbja West"],
        ["Xragba", "Abegujvpx Cnex"],
        ["Xvyohea", "Oebaqrfohel"],
        ["Arj Pebff", "Arj Pebff Gate"],
        ["Gbjre Tngrjnl", "Nyqtngr"],
        ["Juvgr Pvgl", "Jbbq Ynar"]
    ],
    ka = ["Prageny", "Whovyrr", "Ivpgbevn", "Abegurea"],
    la = ["prageny_jbbqsbeq", "abegurea_zvyyuvyyrnfg"],
    ma = [
        ["prageny_znvayvar", "snxr0-ag"],
        ["prageny_znvayvar", "snxr1-ag"],
        ["prageny_znvayvar", "snxr2-ag"],
        ["prageny_znvayvar", "Jrfg Ehvfyvc"],
        ["prageny_znvayvar", "Ehvfyvc Tneqraf"],
        ["prageny_znvayvar", "Fbhgu Ehvfyvc"],
        ["prageny_znvayvar", "Abegubyg"],
        ["prageny_znvayvar", "Terrasbeq"],
        ["prageny_znvayvar", "Crevinyr"],
        ["prageny_znvayvar", "Unatre Ynar"],
        ["prageny_znvayvar", "Qroqra"],
        ["prageny_znvayvar", "Gurlqba Obvf"],
        ["prageny_znvayvar", "Rccvat"],
        ["abegurea_znvayvar", "snxr0-ag"],
        ["abegurea_znvayvar", "snxr1-ag"],
        ["abegurea_znvayvar", "snxr2-ag"],
        ["abegurea_znvayvar", "Ryrcunag & Pnfgyr"],
        ["abegurea_znvayvar", "Obebhtu"],
        ["abegurea_znvayvar", "Ybaqba Oevqtr"],
        ["abegurea_znvayvar", "Onax"],
        ["abegurea_znvayvar", "Zbbetngr"],
        ["abegurea_znvayvar", "Byq Fgerrg"],
        ["abegurea_znvayvar", "Natry"],
        ["abegurea_znvayvar", "Xvat'f Pebff Fg. Cnapenf"],
        ["abegurea_znvayvar", "Rhfgba"]
    ],
    na = [
        ["Urnguebj Grezvanyf 1-2-3", "Urnguebj Grezvany 4"],
        ["Urnguebj Grezvany 4", "Unggba Pebff"],
        ["Errirf Pbeare", "Jnaqyr Cnex"],
        ["Jnaqyr Cnex", "Puhepu Fgerrg"],
        ["Puhepu Fgerrg", "Trbetr Fgerrg"],
        ["Pragenyr", "Puhepu Fgerrg"],
        ["Jryyrfyrl Ebnq", "Jrfg Peblqba"],
        ["Rnfg Peblqba", "Jryyrfyrl Ebnq"]
    ],
    z = [{
            c: "Onxreybb",
            b: "onxreybb_znvayvar",
            a: "gsy-yh",
            f: [
                ["Ryrcunag & Pnfgyr", 0, 12, [.493, .72],
                    [.574, .602],
                    [.609, .676]
                ],
                ["snxr0", 0, 1, [.487, .709]],
                ["Ynzorgu Abegu", 2, 1, [.487, .662],
                    [.558, .589],
                    [.586, .678]
                ],
                ["snxr2", 0, 1, [.566, .677]],
                ["snxr2", 0, 1, [.559, .671]],
                ["Jngreybb", 4, 1, [.487, .611],
                    [.544, .577],
                    [.554, .657]
                ],
                ["Rzonaxzrag", 5, 1, [.487, .577],
                    [.524, .56],
                    [.539, .619]
                ],
                ["snxr0", 0, 1, [.487, .549]],
                ["Punevat Pebff", 6, 1, [.48, .539],
                    [.516, .553],
                    [.533, .606]
                ],
                ["snxr2", 0, 1, [.526, .601]],
                ["snxr2", 0, 1, [.512, .599]],
                ["snxr2", 0, 1, [.506, .594]],
                ["Cvppnqvyyl Pvephf",
                    8, 1, [.458, .508],
                    [.488, .53],
                    [.498, .577]
                ],
                ["snxr0", 0, 1, [.438, .477]],
                ["Bksbeq Pvephf", 10, 1, [.438, .46],
                    [.472, .496],
                    [.479, .531]
                ],
                ["snxr0", 0, 1, [.438, .443]],
                ["snxr2", 0, 1, [.466, .498]],
                ["snxr2", 0, 1, [.461, .492]],
                ["Ertrag'f Cnex", 12, 1, [.424, .425],
                    [.456, .456],
                    [.455, .492]
                ],
                ["snxr1", 0, 1, [.45, .441]],
                ["snxr1", 0, 1, [.445, .437]],
                ["Onxre Fgerrg", 14, 1, [.404, .397],
                    [.43, .436],
                    [.435, .491]
                ],
                ["snxr0", 0, 1, [.389, .375]],
                ["snxr2", 0, 1, [.423, .491]],
                ["snxr1", 0, 0, [.415, .437]],
                ["Znelyrobar", 15, 1, [.375, .375],
                    [.407, .441],
                    [.419, .495]
                ],
                ["Rqtjner Ebnq", 16, 1, [.343, .375],
                    [.397, .452],
                    [.407, .511]
                ],
                ["snxr2", 0, 1, [.402, .519]],
                ["snxr2", 0, 1, [.397, .543]],
                ["snxr1", 0, 1, [.393, .458]],
                ["snxr1", 0, 1, [.387, .47]],
                ["Cnqqvatgba", 18, 1, [.316, .375],
                    [.379, .478],
                    [.393, .554]
                ],
                ["snxr2", 0, 1, [.381, .561]],
                ["snxr2", 0, 1, [.371, .556]],
                ["snxr1", 0, 1, [.373, .482]],
                ["snxr1", 0, 1, [.367, .478]],
                ["Jnejvpx Nirahr", 20, 2, [.281, .375],
                    [.355, .449],
                    [.358, .541]
                ],
                ["Znvqn Inyr", 22, 2, [.268, .371],
                    [.347, .428],
                    [.347, .524]
                ],
                ["Xvyohea Cnex", 24, 2, [.26, .36],
                    [.338, .408],
                    [.335, .508]
                ],
                ["snxr1",
                    0, 0, [.336, .402]
                ],
                ["snxr1", 0, 0, [.33, .398]],
                ["Dhrra'f Cnex", 27, 2, [.257, .339],
                    [.318, .397],
                    [.323, .491]
                ],
                ["Xrafny Terra", 30, 2, [.257, .326],
                    [.286, .398],
                    [.309, .473]
                ],
                ["Jvyyrfqra Whapgvba", 33, 23, [.257, .31],
                    [.253, .398],
                    [.297, .455]
                ],
                ["snxr1", 0, 0, [.241, .398]],
                ["snxr1", 0, 0, [.234, .393]],
                ["Uneyrfqra", 35, 3, [.257, .29],
                    [.226, .374],
                    [.283, .436]
                ],
                ["Fgbaroevqtr Cnex", 38, 3, [.257, .277],
                    [.216, .347],
                    [.272, .42]
                ],
                ["Jrzoyrl Prageny", 40, 4, [.257, .264],
                    [.205, .322],
                    [.262, .405]
                ],
                ["Abegu Jrzoyrl", 42, 4, [.257, .251],
                    [.195, .296],
                    [.249,
                        .39
                    ]
                ],
                ["Fbhgu Xragba", 44, 4, [.257, .237],
                    [.187, .276],
                    [.238, .373]
                ],
                ["snxr2", 0, 0, [.233, .361]],
                ["Xragba", 46, 4, [.257, .183],
                    [.173, .24],
                    [.231, .31]
                ],
                ["Uneebj & Jrnyqfgbar", 48, 5, [.257, .161],
                    [.16, .208],
                    [.232, .286]
                ]
            ]
        }, {
            c: "Prageny",
            b: "prageny_znvayvar",
            a: "gsy-yh",
            f: [
                ["Jrfg Ehvfyvc", 0, 6, [.095, .141],
                    [.071, .292],
                    [.078, .315]
                ],
                ["Ehvfyvc Tneqraf", 2, 5, [.095, .218],
                    [.085, .325],
                    [.079, .364]
                ],
                ["Fbhgu Ehvfyvc", 4, 5, [.095, .259],
                    [.092, .342],
                    [.079, .4]
                ],
                ["snxr2", 0, 0, [.079, .435]],
                ["Abegubyg", 7, 5, [.095, .301],
                    [.099, .36],
                    [.084, .449]
                ],
                ["snxr0", 0, 0, [.095, .329]],
                ["Terrasbeq", 9, 4, [.108, .349],
                    [.107, .381],
                    [.11, .485]
                ],
                ["Crevinyr", 11, 4, [.131, .383],
                    [.115, .401],
                    [.142, .529]
                ],
                ["snxr1", 0, 0, [.118, .407]],
                ["snxr1-ag", 0, 0, [.124, .412]],
                ["Unatre Ynar", 14, 3, [.151, .412],
                    [.155, .412],
                    [.169, .567]
                ],
                ["snxr0-ag", 0, 0, [.207, .489]],
                ["snxr1-ag", 0, 0, [.162, .414]],
                ["snxr1-ag", 0, 0, [.166, .42]],
                ["snxr1-ag", 0, 0, [.187, .473]],
                ["snxr1-ag", 0, 0, [.192, .477]],
                ["Abegu Npgba", 17, 23, [.222, .489],
                    [.221, .479],
                    [.215, .63]
                ],
                ["snxr2", 0, 0, [.226, .643]],
                ["Rnfg Npgba", 19, 2, [.239, .489],
                    [.245, .48],
                    [.253, .643]
                ],
                ["snxr1", 0, 0, [.251, .484]],
                ["snxr1", 0, 0, [.255, .492]],
                ["Juvgr Pvgl", 22, 2, [.261, .489],
                    [.262, .512],
                    [.269, .642]
                ],
                ["snxr1", 0, 0, [.275, .543]],
                ["snxr1", 0, 0, [.282, .551]],
                ["Furcureq'f Ohfu", 26, 2, [.293, .489],
                    [.29, .552],
                    [.309, .643]
                ],
                ["Ubyynaq Cnex", 28, 2, [.313, .489],
                    [.31, .538],
                    [.342, .644]
                ],
                ["snxr1", 0, 0, [.319, .531]],
                ["Abggvat Uvyy Tngr", 29, 12, [.328, .489],
                    [.332, .527],
                    [.369, .642]
                ],
                ["Dhrrafjnl", 31, 1, [.356, .489],
                    [.359, .521],
                    [.391, .643]
                ],
                ["snxr0", 0, 1, [.372, .489]],
                ["snxr2", 0, 1, [.401, .641]],
                ["Ynapnfgre Tngr",
                    33, 1, [.376, .484],
                    [.387, .516],
                    [.417, .619]
                ],
                ["Zneoyr Nepu", 34, 1, [.386, .47],
                    [.424, .507],
                    [.438, .591]
                ],
                ["snxr0", 0, 1, [.394, .46]],
                ["Obaq Fgerrg", 36, 1, [.404, .46],
                    [.455, .5],
                    [.461, .558]
                ],
                ["Bksbeq Pvephf", 37, 1, [.438, .46],
                    [.472, .496],
                    [.479, .532]
                ],
                ["Gbggraunz Pbheg Ebnq", 39, 1, [.48, .46],
                    [.49, .492],
                    [.501, .502]
                ],
                ["snxr2", 0, 1, [.51, .493]],
                ["Ubyobea", 40, 1, [.505, .46],
                    [.523, .484],
                    [.534, .492]
                ],
                ["snxr0", 0, 1, [.523, .46]],
                ["Punaprel Ynar", 42, 1, [.531, .472],
                    [.543, .48],
                    [.55, .492]
                ],
                ["snxr0", 0, 1, [.543, .488]],
                ["snxr2", 0, 1, [.572,
                    .493
                ]],
                ["snxr2", 0, 1, [.578, .5]],
                ["snxr1", 0, 1, [.553, .479]],
                ["snxr1", 0, 1, [.561, .482]],
                ["Fg. Cnhy'f", 44, 1, [.553, .488],
                    [.565, .486],
                    [.582, .506]
                ],
                ["snxr2", 0, 1, [.589, .524]],
                ["snxr2", 0, 1, [.598, .531]],
                ["snxr1", 0, 1, [.576, .493]],
                ["Onax", 45, 1, [.572, .488],
                    [.594, .493],
                    [.609, .523]
                ],
                ["snxr0", 0, 1, [.577, .488]],
                ["Yvirecbby Fgerrg", 47, 1, [.608, .446],
                    [.614, .479],
                    [.643, .478]
                ],
                ["snxr0", 0, 1, [.639, .4]],
                ["snxr2", 0, 1, [.669, .442]],
                ["Orguany Terra", 51, 2, [.693, .4],
                    [.663, .44],
                    [.681, .443]
                ],
                ["snxr2", 0, 0, [.732, .442]],
                ["snxr1", 0, 0, [.68,
                    .428
                ]],
                ["Zvyr Raq", 53, 2, [.724, .4],
                    [.712, .428],
                    [.741, .435]
                ],
                ["snxr0", 0, 0, [.733, .4]],
                ["snxr1", 0, 0, [.734, .428]],
                ["snxr1", 0, 0, [.743, .421]],
                ["Fgengsbeq", 57, 23, [.786, .326],
                    [.772, .347],
                    [.807, .342]
                ],
                ["snxr0", 0, 0, [.794, .312]],
                ["snxr2", 0, 0, [.814, .328]],
                ["snxr2", 0, 0, [.814, .314]],
                ["Yrlgba", 60, 3, [.794, .285],
                    [.796, .288],
                    [.794, .269]
                ],
                ["snxr2", 0, 0, [.786, .243]],
                ["Yrlgbafgbar", 62, 34, [.794, .241],
                    [.814, .242],
                    [.786, .22]
                ],
                ["Fanerfoebbx", 66, 4, [.794, .188],
                    [.83, .204],
                    [.786, .189]
                ],
                ["Fbhgu Jbbqsbeq", 68, 4, [.794, .171],
                    [.842,
                        .174
                    ],
                    [.786, .169]
                ],
                ["Jbbqsbeq", 70, 4, [.794, .153],
                    [.852, .142],
                    [.787, .143]
                ],
                ["Ohpxuhefg Uvyy", 73, 5, [.794, .093],
                    [.867, .111],
                    [.787, .122]
                ],
                ["Ybhtugba", 76, 6, [.794, .078],
                    [.872, .097],
                    [.787, .099]
                ],
                ["Qroqra", 79, 6, [.794, .063],
                    [.879, .08],
                    [.787, .078]
                ],
                ["Gurlqba Obvf", 83, 6, [.794, .048],
                    [.885, .064],
                    [.787, .055]
                ],
                ["Rccvat", 85, 6, [.794, .033],
                    [.898, .032],
                    [.787, .033]
                ]
            ]
        }, {
            c: "Prageny",
            b: "prageny_unvanhyg",
            a: "gsy-yh",
            f: [
                ["Yrlgbafgbar", 0, 34, [.794, .241],
                    [.814, .244],
                    [.786, .22]
                ],
                ["snxr0", 0, 0, [.794, .225]],
                ["snxr0", 0, 0, [.797, .218]],
                ["snxr1", 0, 0, [.823, .222]],
                ["snxr2", 0, 0, [.787, .209]],
                ["snxr2", 0, 0, [.795, .199]],
                ["Jnafgrnq", 2, 4, [.808, .218],
                    [.834, .221],
                    [.799, .198]
                ],
                ["Erqoevqtr", 4, 4, [.822, .218],
                    [.851, .221],
                    [.815, .199]
                ],
                ["Tnagf Uvyy", 6, 4, [.837, .218],
                    [.867, .222],
                    [.831, .198]
                ],
                ["snxr0", 0, 0, [.843, .214]],
                ["Arjohel Cnex", 9, 4, [.843, .203],
                    [.884, .214],
                    [.847, .198]
                ],
                ["snxr2", 0, 0, [.853, .196]],
                ["snxr2", 0, 0, [.859, .187]],
                ["Onexvatfvqr", 11, 4, [.843, .183],
                    [.889, .192],
                    [.861, .177]
                ],
                ["Snveybc", 12, 4, [.843, .167],
                    [.888, .174],
                    [.861, .158]
                ],
                ["Unvanhyg", 14, 4, [.843, .151],
                    [.888, .155],
                    [.861, .141]
                ]
            ]
        }, {
            c: "Prageny",
            b: "prageny_jbbqsbeq",
            a: "gsy-yh",
            f: [
                ["Jbbqsbeq", 0, 4, [.794, .153],
                    [.853, .144],
                    [.787, .144]
                ],
                ["snxr0", 0, 0, [.794, .137]],
                ["snxr0", 0, 0, [.798, .132]],
                ["snxr1", 0, 0, [.86, .129]],
                ["snxr1", 0, 0, [.866, .125]],
                ["snxr2", 0, 0, [.79, .129]],
                ["snxr2", 0, 0, [.795, .117]],
                ["Ebqvat Inyyrl", 2, 4, [.808, .132],
                    [.87, .125],
                    [.8, .116]
                ],
                ["snxr1", 0, 0, [.877, .125]],
                ["Puvtjryy", 5, 4, [.822, .132],
                    [.881, .129],
                    [.823, .116]
                ],
                ["snxr1", 0, 0, [.886, .134]],
                ["Tenatr Uvyy", 7, 4, [.833, .132],
                    [.886, .139],
                    [.846,
                        .117
                    ]
                ],
                ["snxr0", 0, 0, [.839, .132]],
                ["snxr0", 0, 0, [.843, .138]],
                ["snxr2", 0, 0, [.852, .118]],
                ["snxr2", 0, 0, [.857, .123]],
                ["snxr2", 0, 0, [.859, .13]],
                ["Unvanhyg", 10, 4, [.843, .151],
                    [.887, .154],
                    [.86, .14]
                ]
            ]
        }, {
            c: "Prageny",
            b: "prageny_rnyvat",
            a: "gsy-yh",
            f: [
                ["Rnyvat Oebnqjnl", 0, 3, [.15, .489],
                    [.155, .504],
                    [.156, .639]
                ],
                ["snxr2", 0, 0, [.169, .638]],
                ["snxr1", 0, 0, [.169, .502]],
                ["snxr1", 0, 0, [.176, .496]],
                ["snxr1", 0, 0, [.179, .485]],
                ["Jrfg Npgba", 2, 3, [.184, .489],
                    [.184, .482],
                    [.184, .622]
                ],
                ["snxr2", 0, 0, [.193, .611]],
                ["snxr2", 0, 0, [.201, .612]],
                ["snxr1", 0, 0, [.193, .479]],
                ["Abegu Npgba", 5, 23, [.222, .489],
                    [.221, .479],
                    [.215, .631]
                ]
            ]
        }, {
            c: "Pvepyr",
            b: "pvepyr_znvayvar",
            a: "gsy-yh",
            f: [
                ["Unzzrefzvgu", 0, 2, [.272, .557],
                    [.264, .59],
                    [.276, .726]
                ],
                ["Tbyqunjx Ebnq", 1, 2, [.272, .544],
                    [.264, .576],
                    [.276, .703]
                ],
                ["Furcureq'f Ohfu Znexrg", 3, 2, [.272, .527],
                    [.263, .559],
                    [.276, .68]
                ],
                ["Jbbq Ynar", 4, 2, [.272, .507],
                    [.264, .534],
                    [.276, .657]
                ],
                ["snxr2", 0, 0, [.278, .632]],
                ["Yngvzre Ebnq", 6, 2, [.272, .457],
                    [.292, .509],
                    [.282, .621]
                ],
                ["Ynqoebxr Tebir", 7, 2, [.272, .442],
                    [.313, .491],
                    [.296, .605]
                ],
                ["snxr0", 0, 0, [.272, .427]],
                ["Jrfgobhear Cnex", 9, 2, [.275, .42],
                    [.327, .479],
                    [.309, .583]
                ],
                ["snxr2", 0, 0, [.325, .566]],
                ["Eblny Bnx", 10, 2, [.285, .405],
                    [.341, .467],
                    [.339, .567]
                ],
                ["snxr0", 0, 1, [.299, .385]],
                ["snxr2", 0, 1, [.37, .564]],
                ["snxr1", 0, 0, [.348, .464]],
                ["Cnqqvatgba", 12, 1, [.323, .386],
                    [.373, .465],
                    [.387, .543]
                ],
                ["snxr2", 0, 1, [.402, .531]],
                ["snxr1", 0, 0, [.394, .464]],
                ["Rqtjner Ebnq", 16, 1, [.357, .386],
                    [.399, .46],
                    [.412, .523]
                ],
                ["snxr1", 0, 0, [.417, .448]],
                ["Onxre Fgerrg", 19, 1, [.413, .386],
                    [.434, .448],
                    [.436, .487]
                ],
                ["snxr1",
                    0, 0, [.457, .447]
                ],
                ["Terng Cbegynaq Fgerrg", 21, 1, [.444, .386],
                    [.462, .443],
                    [.464, .451]
                ],
                ["Rhfgba Fdhner", 23, 1, [.502, .386],
                    [.48, .431],
                    [.499, .406]
                ],
                ["snxr2", 0, 1, [.516, .396]],
                ["Xvat'f Pebff Fg. Cnapenf", 25, 1, [.522, .386],
                    [.508, .405],
                    [.533, .398]
                ],
                ["snxr0", 0, 1, [.532, .386]],
                ["snxr2", 0, 1, [.546, .406]],
                ["snxr2", 0, 1, [.557, .418]],
                ["snxr1", 0, 1, [.517, .401]],
                ["snxr1", 0, 1, [.519, .401]],
                ["Sneevatqba", 28, 1, [.547, .408],
                    [.558, .433],
                    [.572, .448]
                ],
                ["Oneovpna", 30, 1, [.561, .428],
                    [.575, .448],
                    [.584, .473]
                ],
                ["snxr0", 0, 1, [.576, .447]],
                ["Zbbetngr", 32, 1, [.582, .447],
                    [.595, .463],
                    [.609, .478]
                ],
                ["Yvirecbby Fgerrg", 34, 1, [.608, .447],
                    [.615, .48],
                    [.643, .48]
                ],
                ["snxr0", 0, 1, [.636, .448]],
                ["snxr0", 0, 1, [.642, .455]],
                ["snxr2", 0, 1, [.651, .482]],
                ["snxr2", 0, 1, [.656, .493]],
                ["snxr1", 0, 1, [.625, .488]],
                ["Nyqtngr", 38, 1, [.642, .48],
                    [.626, .494],
                    [.658, .507]
                ],
                ["snxr0", 0, 1, [.642, .506]],
                ["snxr0", 0, 1, [.637, .513]],
                ["snxr2", 0, 1, [.656, .526]],
                ["snxr2", 0, 1, [.648, .535]],
                ["snxr1", 0, 1, [.627, .51]],
                ["Gbjre Uvyy", 40, 1, [.63, .513],
                    [.626, .514],
                    [.642, .538]
                ],
                ["snxr1", 0, 1, [.623, .516]],
                ["Zbahzrag", 41, 1, [.59, .513],
                    [.601, .517],
                    [.615, .539]
                ],
                ["snxr0", 0, 1, [.569, .513]],
                ["snxr2", 0, 1, [.596, .538]],
                ["Pnaaba Fgerrg", 42, 1, [.561, .527],
                    [.586, .516],
                    [.59, .544]
                ],
                ["Znafvba Ubhfr", 44, 1, [.55, .541],
                    [.572, .517],
                    [.577, .562]
                ],
                ["Oynpxsevnef", 45, 1, [.54, .557],
                    [.552, .517],
                    [.566, .576]
                ],
                ["snxr0", 0, 1, [.525, .576]],
                ["Grzcyr", 47, 1, [.517, .576],
                    [.54, .518],
                    [.552, .596]
                ],
                ["Rzonaxzrag", 49, 1, [.487, .576],
                    [.524, .556],
                    [.538, .617]
                ],
                ["snxr1", 0, 1, [.515, .577]],
                ["Jrfgzvafgre", 51, 1, [.459, .576],
                    [.51, .581],
                    [.512, .653]
                ],
                ["Fg. Wnzrf'f Cnex",
                    52, 1, [.434, .576],
                    [.492, .596],
                    [.501, .669]
                ],
                ["snxr1", 0, 0, [.479, .605]],
                ["Ivpgbevn", 54, 1, [.417, .576],
                    [.472, .605],
                    [.488, .687]
                ],
                ["Fybnar Fdhner", 57, 1, [.392, .576],
                    [.432, .605],
                    [.473, .709]
                ],
                ["Fbhgu Xrafvatgba", 59, 1, [.362, .576],
                    [.397, .605],
                    [.451, .734]
                ],
                ["Tybhprfgre Ebnq", 62, 1, [.347, .576],
                    [.375, .605],
                    [.427, .745]
                ],
                ["snxr0", 0, 1, [.334, .576]],
                ["snxr0", 0, 1, [.33, .572]],
                ["snxr2", 0, 1, [.411, .743]],
                ["snxr2", 0, 1, [.395, .729]],
                ["snxr2", 0, 1, [.387, .717]],
                ["snxr1", 0, 1, [.364, .605]],
                ["snxr1", 0, 1, [.359, .602]],
                ["snxr1", 0, 1, [.355,
                    .595
                ]],
                ["Uvtu Fgerrg Xrafvatgba", 65, 1, [.33, .525],
                    [.346, .572],
                    [.383, .705]
                ],
                ["snxr2", 0, 1, [.375, .681]],
                ["snxr2", 0, 1, [.371, .659]],
                ["snxr1", 0, 1, [.332, .536]],
                ["Abggvat Uvyy Tngr", 67, 12, [.33, .489],
                    [.332, .528],
                    [.371, .643]
                ],
                ["snxr2", 0, 1, [.373, .62]],
                ["snxr1", 0, 1, [.335, .523]],
                ["Onlfjngre", 69, 1, [.33, .44],
                    [.356, .505],
                    [.375, .601]
                ],
                ["snxr0", 0, 1, [.33, .405]],
                ["snxr2", 0, 1, [.381, .58]],
                ["Cnqqvatgba F", 71, 1, [.332, .398],
                    [.381, .485],
                    [.394, .557]
                ],
                ["snxr0", 0, 1, [.338, .394]],
                ["Rqtjner Ebnq F", 76, 1, [.357, .394],
                    [.402, .465],
                    [.414,
                        .53
                    ]
                ]
            ]
        }, {
            c: "Qvfgevpg",
            b: "qvfgevpg_znvayvar",
            a: "gsy-yh",
            f: [
                ["Rnyvat Oebnqjnl", 0, 3, [.15, .492],
                    [.155, .506],
                    [.157, .645]
                ],
                ["snxr0", 0, 0, [.16, .492]],
                ["snxr0", 0, 0, [.164, .501]],
                ["snxr2", 0, 0, [.171, .648]],
                ["snxr1", 0, 0, [.165, .507]],
                ["snxr1", 0, 0, [.169, .511]],
                ["snxr1", 0, 0, [.173, .519]],
                ["Rnyvat Pbzzba", 3, 3, [.164, .533],
                    [.174, .533],
                    [.182, .659]
                ],
                ["snxr0", 0, 0, [.164, .559]],
                ["snxr1", 0, 0, [.175, .545]],
                ["Npgba Gbja", 6, 3, [.17, .568],
                    [.183, .565],
                    [.198, .682]
                ],
                ["snxr0", 0, 0, [.177, .578]],
                ["snxr1", 0, 0, [.194, .593]],
                ["Puvfjvpx Cnex",
                    8, 3, [.185, .578],
                    [.196, .597],
                    [.21, .7]
                ],
                ["snxr1", 0, 0, [.201, .602]],
                ["Gheaunz Terra", 10, 23, [.204, .578],
                    [.213, .601],
                    [.228, .724]
                ],
                ["Fgnzsbeq Oebbx", 12, 2, [.227, .578],
                    [.227, .601],
                    [.238, .739]
                ],
                ["snxr2", 0, 0, [.247, .748]],
                ["Enirafpbheg Cnex", 13, 2, [.253, .578],
                    [.247, .602],
                    [.256, .75]
                ],
                ["Unzzrefzvgu", 16, 2, [.271, .578],
                    [.262, .602],
                    [.274, .749]
                ],
                ["snxr1", 0, 0, [.272, .603]],
                ["Onebaf Pbheg", 17, 2, [.287, .578],
                    [.288, .616],
                    [.302, .751]
                ],
                ["snxr1", 0, 0, [.304, .627]],
                ["Jrfg Xrafvatgba", 19, 2, [.303, .578],
                    [.31, .629],
                    [.332, .751]
                ],
                ["snxr1",
                    0, 0, [.33, .629]
                ],
                ["Rney'f Pbheg", 22, 12, [.322, .578],
                    [.345, .619],
                    [.367, .75]
                ],
                ["snxr1", 0, 1, [.36, .609]],
                ["Tybhprfgre Ebnq", 25, 1, [.347, .578],
                    [.375, .609],
                    [.426, .748]
                ],
                ["snxr2", 0, 1, [.44, .748]],
                ["Fbhgu Xrafvatgba", 26, 1, [.362, .578],
                    [.398, .609],
                    [.452, .739]
                ],
                ["Fybnar Fdhner", 29, 1, [.392, .578],
                    [.432, .61],
                    [.473, .713]
                ],
                ["Ivpgbevn", 31, 1, [.417, .578],
                    [.473, .609],
                    [.49, .689]
                ],
                ["snxr1", 0, 1, [.48, .609]],
                ["Fg. Wnzrf'f Cnex", 32, 1, [.434, .578],
                    [.493, .599],
                    [.503, .674]
                ],
                ["Jrfgzvafgre", 34, 1, [.459, .578],
                    [.511, .584],
                    [.513, .656]
                ],
                ["snxr1", 0, 1, [.517, .578]],
                ["Rzonaxzrag", 36, 1, [.487, .578],
                    [.525, .56],
                    [.539, .622]
                ],
                ["snxr1", 0, 1, [.54, .525]],
                ["Grzcyr", 38, 1, [.517, .578],
                    [.542, .522],
                    [.553, .599]
                ],
                ["snxr0", 0, 1, [.527, .578]],
                ["snxr1", 0, 1, [.544, .521]],
                ["Oynpxsevnef", 40, 1, [.541, .558],
                    [.552, .521],
                    [.567, .581]
                ],
                ["Znafvba Ubhfr", 42, 1, [.552, .544],
                    [.572, .522],
                    [.579, .564]
                ],
                ["Pnaaba Fgerrg", 43, 1, [.563, .527],
                    [.587, .522],
                    [.591, .548]
                ],
                ["snxr0", 0, 1, [.571, .515]],
                ["snxr2", 0, 1, [.599, .542]],
                ["Zbahzrag", 45, 1, [.59, .515],
                    [.601, .521],
                    [.615, .542]
                ],
                ["snxr1", 0,
                    1, [.622, .521]
                ],
                ["Gbjre Uvyy", 47, 1, [.63, .515],
                    [.626, .517],
                    [.642, .542]
                ],
                ["snxr0", 0, 1, [.637, .515]],
                ["snxr0", 0, 1, [.644, .506]],
                ["snxr0", 0, 1, [.644, .48]],
                ["snxr2", 0, 1, [.648, .539]],
                ["snxr2", 0, 1, [.664, .523]],
                ["snxr1", 0, 1, [.629, .512]],
                ["snxr1", 0, 1, [.63, .5]],
                ["snxr1", 0, 1, [.633, .494]],
                ["Nyqtngr Rnfg", 50, 1, [.662, .457],
                    [.64, .485],
                    [.678, .522]
                ],
                ["snxr2", 0, 1, [.687, .518]],
                ["Juvgrpuncry", 52, 2, [.671, .444],
                    [.652, .476],
                    [.697, .502]
                ],
                ["Fgrcarl Terra", 54, 2, [.691, .417],
                    [.685, .45],
                    [.719, .473]
                ],
                ["snxr0", 0, 0, [.698, .412]],
                ["snxr1",
                    0, 0, [.702, .436]
                ],
                ["Zvyr Raq", 56, 2, [.724, .412],
                    [.713, .437],
                    [.742, .439]
                ],
                ["snxr2", 0, 0, [.752, .431]],
                ["Obj Ebnq", 57, 2, [.756, .412],
                    [.741, .437],
                    [.767, .432]
                ],
                ["Oebzyrl-ol-Obj", 59, 23, [.783, .412],
                    [.765, .436],
                    [.801, .432]
                ],
                ["snxr1", 0, 0, [.772, .435]],
                ["Jrfg Unz", 62, 23, [.801, .412],
                    [.8, .411],
                    [.828, .432]
                ],
                ["snxr0", 0, 0, [.819, .412]],
                ["snxr2", 0, 0, [.837, .43]],
                ["Cynvfgbj", 63, 3, [.826, .402],
                    [.82, .396],
                    [.845, .423]
                ],
                ["Hcgba Cnex", 65, 3, [.839, .384],
                    [.841, .379],
                    [.857, .408]
                ],
                ["Rnfg Unz", 68, 34, [.851, .366],
                    [.859, .362],
                    [.869, .39]
                ],
                ["snxr1", 0, 0, [.874, .351]],
                ["Onexvat", 72, 4, [.863, .351],
                    [.887, .351],
                    [.88, .374]
                ],
                ["Hcarl", 74, 4, [.873, .337],
                    [.904, .35],
                    [.893, .356]
                ],
                ["snxr1", 0, 0, [.907, .35]],
                ["Orpbagerr", 77, 5, [.883, .323],
                    [.917, .341],
                    [.904, .34]
                ],
                ["Qntraunz Urngujnl", 79, 5, [.894, .308],
                    [.926, .334],
                    [.916, .323]
                ],
                ["Qntraunz Rnfg", 81, 5, [.905, .291],
                    [.936, .325],
                    [.927, .307]
                ],
                ["Ryz Cnex", 84, 6, [.921, .268],
                    [.944, .318],
                    [.94, .291]
                ],
                ["Ubeapuhepu", 87, 6, [.93, .255],
                    [.954, .311],
                    [.951, .273]
                ],
                ["Hczvafgre Oevqtr", 89, 6, [.941, .241],
                    [.963, .303],
                    [.964, .256]
                ],
                ["Hczvafgre",
                    91, 6, [.952, .225],
                    [.977, .291],
                    [.975, .241]
                ]
            ]
        }, {
            c: "Qvfgevpg",
            b: "qvfgevpg_evpuzbaq",
            a: "gsy-yh",
            f: [
                ["Evpuzbaq", 0, 4, [.191, .675],
                    [.14, .735],
                    [.165, .775]
                ],
                ["Xrj Tneqraf", 3, 34, [.191, .654],
                    [.166, .669],
                    [.182, .755]
                ],
                ["Thaarefohel", 6, 3, [.191, .614],
                    [.183, .624],
                    [.2, .727]
                ],
                ["snxr0", 0, 0, [.191, .584]],
                ["snxr0", 0, 0, [.193, .578]],
                ["snxr2", 0, 0, [.209, .717]],
                ["snxr2", 0, 0, [.22, .715]],
                ["snxr1", 0, 0, [.19, .607]],
                ["snxr1", 0, 0, [.196, .601]],
                ["Gheaunz Terra", 9, 23, [.204, .578],
                    [.214, .601],
                    [.227, .726]
                ]
            ]
        }, {
            c: "Qvfgevpg",
            b: "qvfgevpg_jvzoyrqba",
            a: "gsy-yh",
            f: [
                ["Jvzoyrqba", 0, 3, [.316, .761],
                    [.347, .815],
                    [.394, .935]
                ],
                ["Jvzoyrqba Cnex", 2, 3, [.316, .744],
                    [.338, .792],
                    [.382, .918]
                ],
                ["Fbhgusvryqf", 5, 3, [.316, .729],
                    [.331, .776],
                    [.37, .901]
                ],
                ["Rnfg Chgarl", 7, 23, [.316, .712],
                    [.325, .762],
                    [.358, .883]
                ],
                ["snxr2", 0, 0, [.352, .87]],
                ["snxr1", 0, 0, [.317, .742]],
                ["snxr1", 0, 0, [.317, .733]],
                ["Chgarl Oevqtr", 10, 2, [.316, .69],
                    [.319, .723],
                    [.352, .861]
                ],
                ["Cnefbaf Terra", 12, 2, [.316, .675],
                    [.327, .703],
                    [.351, .838]
                ],
                ["Shyunz Oebnqjnl", 14, 2, [.316, .66],
                    [.336, .683],
                    [.352, .815]
                ],
                ["snxr1",
                    0, 0, [.343, .665]
                ],
                ["snxr1", 0, 0, [.343, .657]],
                ["Jrfg Oebzcgba", 16, 2, [.316, .622],
                    [.34, .651],
                    [.352, .787]
                ],
                ["snxr0", 0, 0, [.316, .584]],
                ["snxr2", 0, 0, [.351, .771]],
                ["snxr2", 0, 0, [.354, .761]],
                ["snxr2", 0, 0, [.362, .751]],
                ["snxr1", 0, 0, [.337, .642]],
                ["snxr1", 0, 0, [.337, .633]],
                ["snxr1", 0, 0, [.341, .624]],
                ["Rney'f Pbheg", 19, 12, [.322, .578],
                    [.345, .621],
                    [.368, .751]
                ]
            ]
        }, {
            c: "Qvfgevpg",
            b: "qvfgevpg_rqtjner",
            a: "gsy-yh",
            f: [
                ["Rney'f Pbheg", 0, 12, [.322, .578],
                    [.345, .619],
                    [.367, .75]
                ],
                ["snxr0", 0, 1, [.325, .578]],
                ["snxr0", 0, 1, [.328, .572]],
                ["snxr2", 0, 1, [.377, .745]],
                ["snxr2", 0, 1, [.383, .731]],
                ["snxr2", 0, 1, [.383, .716]],
                ["snxr1", 0, 1, [.352, .611]],
                ["snxr1", 0, 1, [.354, .601]],
                ["snxr1", 0, 1, [.353, .595]],
                ["Uvtu Fgerrg Xrafvatgba", 3, 1, [.328, .525],
                    [.343, .573],
                    [.381, .707]
                ],
                ["snxr1", 0, 1, [.33, .536]],
                ["Abggvat Uvyy Tngr", 5, 12, [.328, .489],
                    [.33, .527],
                    [.368, .642]
                ],
                ["snxr1", 0, 1, [.332, .519]],
                ["Onlfjngre", 7, 1, [.328, .44],
                    [.356, .503],
                    [.373, .599]
                ],
                ["snxr0", 0, 1, [.328, .403]],
                ["snxr2", 0, 1, [.381, .579]],
                ["Cnqqvatgba", 9, 1, [.331, .397],
                    [.38, .48],
                    [.393, .555]
                ],
                ["snxr0",
                    0, 1, [.336, .391]
                ],
                ["Rqtjner Ebnq F", 12, 1, [.357, .391],
                    [.401, .462],
                    [.413, .525]
                ]
            ]
        }, {
            c: "Qvfgevpg",
            b: "qvfgevpg_bylzcvn",
            a: "gsy-yh",
            f: [
                ["Xrafvatgba (Bylzcvn)", 0, 2, [.316, .534],
                    [.309, .587],
                    [.351, .711]
                ],
                ["snxr0", 0, 0, [.316, .572]],
                ["snxr0", 0, 0, [.322, .578]],
                ["snxr2", 0, 0, [.352, .727]],
                ["snxr2", 0, 0, [.355, .737]],
                ["snxr2", 0, 0, [.361, .748]],
                ["snxr1", 0, 0, [.323, .623]],
                ["snxr1", 0, 0, [.33, .628]],
                ["snxr1", 0, 0, [.336, .627]],
                ["Rney'f Pbheg", 6, 12, [.322, .578],
                    [.345, .619],
                    [.368, .749]
                ]
            ]
        }, {
            c: "UnzzrefzvguPvgl",
            b: "unzzrefzvgu_znvayvar",
            a: "gsy-yh",
            f: [
                ["Unzzrefzvgu", 0, 2, [.27, .557],
                    [.26, .591],
                    [.272, .725]
                ],
                ["Tbyqunjx Ebnq", 1, 2, [.27, .544],
                    [.261, .575],
                    [.273, .703]
                ],
                ["Furcureq'f Ohfu Znexrg", 3, 2, [.27, .527],
                    [.26, .56],
                    [.273, .681]
                ],
                ["snxr1", 0, 0, [.261, .536]],
                ["Jbbq Ynar", 4, 2, [.27, .507],
                    [.262, .532],
                    [.273, .656]
                ],
                ["snxr2", 0, 0, [.274, .633]],
                ["snxr1", 0, 0, [.264, .528]],
                ["Yngvzre Ebnq", 6, 2, [.27, .457],
                    [.291, .506],
                    [.28, .618]
                ],
                ["Ynqoebxr Tebir", 7, 2, [.27, .442],
                    [.312, .488],
                    [.295, .601]
                ],
                ["snxr0", 0, 0, [.27, .425]],
                ["Jrfgobhear Cnex", 9, 2, [.274, .418],
                    [.325, .476],
                    [.308, .58]
                ],
                ["snxr2", 0, 0, [.322, .563]],
                ["Eblny Bnx", 10, 2, [.284, .403],
                    [.34, .465],
                    [.339, .562]
                ],
                ["snxr0", 0, 1, [.298, .383]],
                ["snxr2", 0, 1, [.369, .56]],
                ["snxr1", 0, 1, [.348, .462]],
                ["Cnqqvatgba", 12, 1, [.323, .383],
                    [.372, .461],
                    [.385, .54]
                ],
                ["snxr2", 0, 1, [.392, .53]],
                ["snxr1", 0, 0, [.394, .462]],
                ["Rqtjner Ebnq", 16, 1, [.357, .383],
                    [.399, .458],
                    [.41, .52]
                ],
                ["snxr1", 0, 1, [.418, .445]],
                ["Onxre Fgerrg", 20, 1, [.413, .383],
                    [.434, .444],
                    [.434, .485]
                ],
                ["snxr1", 0, 1, [.454, .444]],
                ["Terng Cbegynaq Fgerrg", 22, 1, [.444, .383],
                    [.461, .441],
                    [.463,
                        .446
                    ]
                ],
                ["Rhfgba Fdhner", 23, 1, [.502, .383],
                    [.478, .427],
                    [.498, .403]
                ],
                ["snxr2", 0, 1, [.516, .392]],
                ["Xvat'f Pebff Fg. Cnapenf", 25, 1, [.522, .383],
                    [.507, .402],
                    [.535, .393]
                ],
                ["snxr0", 0, 1, [.533, .383]],
                ["snxr2", 0, 1, [.549, .403]],
                ["snxr2", 0, 1, [.563, .421]],
                ["snxr1", 0, 1, [.515, .397]],
                ["snxr1", 0, 1, [.52, .398]],
                ["Sneevatqba", 29, 1, [.548, .405],
                    [.559, .429],
                    [.574, .447]
                ],
                ["Oneovpna", 30, 1, [.563, .425],
                    [.576, .444],
                    [.585, .469]
                ],
                ["snxr0", 0, 1, [.576, .443]],
                ["snxr2", 0, 1, [.595, .473]],
                ["Zbbetngr", 32, 1, [.582, .443],
                    [.595, .46],
                    [.61, .473]
                ],
                ["Yvirecbby Fgerrg", 34, 1, [.608, .443],
                    [.617, .478],
                    [.643, .473]
                ],
                ["snxr0", 0, 1, [.638, .443]],
                ["snxr0", 0, 1, [.653, .462]],
                ["snxr2", 0, 1, [.652, .478]],
                ["snxr2", 0, 1, [.663, .503]],
                ["snxr2", 0, 1, [.668, .512]],
                ["snxr1", 0, 1, [.627, .486]],
                ["snxr1", 0, 1, [.635, .487]],
                ["Nyqtngr Rnfg", 38, 1, [.661, .455],
                    [.639, .482],
                    [.676, .517]
                ],
                ["Juvgrpuncry", 40, 2, [.671, .441],
                    [.651, .472],
                    [.695, .5]
                ],
                ["Fgrcarl Terra", 42, 2, [.689, .415],
                    [.685, .446],
                    [.717, .469]
                ],
                ["snxr0", 0, 0, [.697, .409]],
                ["snxr1", 0, 0, [.7, .433]],
                ["snxr1", 0, 0, [.707, .432]],
                ["Zvyr Raq",
                    44, 2, [.724, .409],
                    [.712, .43],
                    [.741, .437]
                ],
                ["snxr2", 0, 0, [.752, .426]],
                ["Obj Ebnq", 46, 2, [.756, .409],
                    [.741, .432],
                    [.768, .426]
                ],
                ["Oebzyrl-ol-Obj", 48, 23, [.783, .409],
                    [.765, .432],
                    [.801, .426]
                ],
                ["snxr1", 0, 0, [.77, .432]],
                ["Jrfg Unz", 50, 23, [.801, .409],
                    [.799, .407],
                    [.828, .426]
                ],
                ["snxr0", 0, 0, [.818, .409]],
                ["snxr2", 0, 0, [.836, .423]],
                ["Cynvfgbj", 52, 3, [.825, .4],
                    [.819, .391],
                    [.844, .42]
                ],
                ["Hcgba Cnex", 54, 3, [.838, .382],
                    [.84, .374],
                    [.856, .403]
                ],
                ["Rnfg Unz", 56, 34, [.85, .364],
                    [.858, .358],
                    [.867, .386]
                ],
                ["snxr1", 0, 0, [.873, .347]],
                ["Onexvat",
                    60, 4, [.862, .349],
                    [.887, .346],
                    [.876, .369]
                ]
            ]
        }, {
            c: "Whovyrr",
            b: "whovyrr_znvayvar",
            a: "gsy-yh",
            f: [
                ["Fgnazber", 0, 5, [.309, .152],
                    [.237, .122],
                    [.28, .23]
                ],
                ["Pnabaf Cnex", 2, 5, [.309, .17],
                    [.237, .156],
                    [.28, .251]
                ],
                ["Dhrrafohel", 4, 4, [.309, .187],
                    [.237, .186],
                    [.28, .272]
                ],
                ["Xvatfohel", 7, 4, [.309, .205],
                    [.237, .215],
                    [.28, .295]
                ],
                ["snxr0", 0, 0, [.309, .22]],
                ["snxr1", 0, 0, [.237, .232]],
                ["snxr1", 0, 0, [.24, .24]],
                ["Jrzoyrl Cnex", 11, 4, [.314, .23],
                    [.26, .258],
                    [.28, .324]
                ],
                ["snxr2", 0, 0, [.283, .336]],
                ["Arnfqra", 14, 3, [.325, .245],
                    [.284, .279],
                    [.292,
                        .336
                    ]
                ],
                ["Qbyyvf Uvyy", 15, 3, [.336, .261],
                    [.298, .291],
                    [.316, .338]
                ],
                ["snxr1", 0, 0, [.312, .301]],
                ["Jvyyrfqra Terra", 18, 23, [.347, .276],
                    [.319, .302],
                    [.34, .337]
                ],
                ["Xvyohea", 19, 2, [.358, .293],
                    [.338, .303],
                    [.363, .336]
                ],
                ["snxr2", 0, 0, [.369, .338]],
                ["snxr2", 0, 0, [.374, .344]],
                ["Jrfg Unzcfgrnq", 21, 2, [.376, .318],
                    [.366, .301],
                    [.383, .368]
                ],
                ["snxr1", 0, 0, [.373, .304]],
                ["snxr1", 0, 0, [.378, .308]],
                ["Svapuyrl Ebnq", 23, 2, [.383, .328],
                    [.382, .318],
                    [.393, .39]
                ],
                ["Fjvff Pbggntr", 24, 2, [.391, .339],
                    [.396, .352],
                    [.402, .412]
                ],
                ["Fg. Wbua'f Jbbq",
                    26, 2, [.4, .352],
                    [.41, .387],
                    [.416, .449]
                ],
                ["snxr0", 0, 1, [.404, .359]],
                ["Onxre Fgerrg", 29, 1, [.404, .398],
                    [.431, .437],
                    [.434, .49]
                ],
                ["Obaq Fgerrg", 31, 1, [.404, .461],
                    [.456, .5],
                    [.461, .558]
                ],
                ["snxr0", 0, 1, [.404, .487]],
                ["Terra Cnex", 33, 1, [.417, .508],
                    [.473, .542],
                    [.479, .604]
                ],
                ["snxr2", 0, 1, [.498, .647]],
                ["snxr2", 0, 1, [.504, .654]],
                ["snxr1", 0, 0, [.477, .553]],
                ["Jrfgzvafgre", 35, 1, [.459, .566],
                    [.51, .582],
                    [.513, .655]
                ],
                ["snxr0", 0, 1, [.473, .586]],
                ["snxr1", 0, 0, [.539, .583]],
                ["Jngreybb", 37, 1, [.473, .611],
                    [.546, .577],
                    [.554, .659]
                ],
                ["snxr0",
                    0, 1, [.473, .622]
                ],
                ["snxr0", 0, 1, [.478, .628]],
                ["snxr2", 0, 1, [.56, .654]],
                ["Fbhgujnex", 38, 1, [.51, .628],
                    [.567, .562],
                    [.574, .635]
                ],
                ["snxr0", 0, 1, [.537, .628]],
                ["snxr0", 0, 1, [.573, .579]],
                ["snxr2", 0, 1, [.595, .607]],
                ["snxr1", 0, 0, [.583, .55]],
                ["Ybaqba Oevqtr", 40, 1, [.582, .579],
                    [.595, .549],
                    [.609, .606]
                ],
                ["snxr1", 0, 0, [.608, .551]],
                ["Orezbaqfrl", 42, 2, [.628, .579],
                    [.643, .579],
                    [.654, .607]
                ],
                ["snxr1", 0, 0, [.663, .593]],
                ["snxr1", 0, 0, [.689, .593]],
                ["Pnanqn Jngre", 44, 2, [.671, .579],
                    [.697, .586],
                    [.697, .607]
                ],
                ["snxr2", 0, 0, [.708, .604]],
                ["snxr2", 0, 0, [.723, .587]],
                ["snxr1", 0, 0, [.736, .556]],
                ["Pnanel Junes", 47, 2, [.739, .579],
                    [.748, .554],
                    [.773, .587]
                ],
                ["snxr0", 0, 0, [.778, .579]],
                ["Abegu Terrajvpu", 49, 23, [.785, .572],
                    [.804, .555],
                    [.816, .587]
                ],
                ["snxr0", 0, 0, [.796, .555]],
                ["snxr2", 0, 0, [.821, .587]],
                ["snxr2", 0, 0, [.826, .578]],
                ["snxr1", 0, 0, [.811, .55]],
                ["snxr1", 0, 0, [.815, .54]],
                ["Pnaavat Gbja", 52, 23, [.796, .512],
                    [.815, .502],
                    [.826, .539]
                ],
                ["snxr1", 0, 0, [.814, .454]],
                ["Jrfg Unz", 55, 23, [.796, .403],
                    [.798, .412],
                    [.826, .429]
                ],
                ["snxr0", 0, 0, [.796, .341]],
                ["snxr2",
                    0, 0, [.825, .396]
                ],
                ["Fgengsbeq", 58, 23, [.806, .326],
                    [.772, .347],
                    [.81, .347]
                ]
            ]
        }, {
            c: "Zrgebcbyvgna",
            b: "zrgebcbyvgna_znvayvar",
            a: "gsy-yh",
            f: [
                ["Nzrefunz", 0, 9, [.022, .065],
                    [.022, .085],
                    [.061, .152]
                ],
                ["snxr1", 0, 0, [.049, .086]],
                ["snxr1", 0, 0, [.054, .087]],
                ["Punysbag & Yngvzre", 4, 8, [.077, .065],
                    [.059, .096],
                    [.077, .176]
                ],
                ["snxr0", 0, 0, [.101, .065]],
                ["Pubeyrljbbq", 9, 7, [.111, .081],
                    [.071, .125],
                    [.089, .191]
                ],
                ["Evpxznafjbegu", 14, 7, [.125, .101],
                    [.081, .15],
                    [.101, .209]
                ],
                ["Zbbe Cnex", 19, 67, [.141, .123],
                    [.092, .179],
                    [.117, .232]
                ],
                ["Abegujbbq",
                    21, 6, [.152, .139],
                    [.101, .201],
                    [.128, .248]
                ],
                ["Abegujbbq Uvyyf", 24, 6, [.163, .154],
                    [.107, .216],
                    [.141, .264]
                ],
                ["Cvaare", 26, 5, [.176, .172],
                    [.112, .23],
                    [.152, .28]
                ],
                ["Abegu Uneebj", 28, 5, [.185, .185],
                    [.118, .243],
                    [.164, .298]
                ],
                ["snxr0", 0, 0, [.205, .211]],
                ["snxr2", 0, 0, [.186, .325]],
                ["snxr1", 0, 0, [.121, .25]],
                ["snxr1", 0, 0, [.129, .256]],
                ["Uneebj-ba-gur-Uvyy", 32, 5, [.214, .211],
                    [.14, .255],
                    [.196, .325]
                ],
                ["Abegujvpx Cnex", 34, 4, [.241, .211],
                    [.164, .257],
                    [.227, .324]
                ],
                ["Cerfgba Ebnq", 37, 4, [.276, .211],
                    [.213, .257],
                    [.256, .327]
                ],
                ["snxr0",
                    0, 0, [.291, .211]
                ],
                ["snxr1", 0, 0, [.245, .257]],
                ["Jrzoyrl Cnex", 39, 4, [.309, .238],
                    [.257, .264],
                    [.28, .325]
                ],
                ["snxr2", 0, 0, [.367, .326]],
                ["snxr2", 0, 0, [.379, .336]],
                ["snxr1", 0, 0, [.311, .31]],
                ["snxr1", 0, 0, [.367, .311]],
                ["snxr1", 0, 0, [.372, .315]],
                ["snxr1", 0, 0, [.375, .319]],
                ["Svapuyrl Ebnq", 46, 2, [.378, .335],
                    [.377, .324],
                    [.397, .383]
                ],
                ["snxr1", 0, 1, [.425, .445]],
                ["snxr1", 0, 1, [.428, .45]],
                ["Onxre Fgerrg", 54, 1, [.413, .39],
                    [.434, .45],
                    [.438, .484]
                ],
                ["snxr2", 0, 1, [.446, .484]],
                ["snxr1", 0, 1, [.458, .451]],
                ["Terng Cbegynaq Fgerrg", 56,
                    1, [.444, .39],
                    [.464, .448],
                    [.465, .457]
                ],
                ["Rhfgba Fdhner", 57, 1, [.502, .39],
                    [.48, .433],
                    [.5, .41]
                ],
                ["snxr2", 0, 1, [.517, .401]],
                ["Xvat'f Pebff Fg. Cnapenf", 59, 1, [.522, .39],
                    [.509, .41],
                    [.533, .403]
                ],
                ["snxr0", 0, 1, [.53, .39]],
                ["snxr2", 0, 1, [.55, .415]],
                ["snxr2", 0, 1, [.563, .435]],
                ["snxr1", 0, 1, [.516, .405]],
                ["snxr1", 0, 1, [.519, .405]],
                ["Sneevatqba", 63, 1, [.545, .411],
                    [.557, .437],
                    [.571, .451]
                ],
                ["Oneovpna", 64, 1, [.559, .43],
                    [.574, .452],
                    [.583, .476]
                ],
                ["snxr0", 0, 1, [.575, .45]],
                ["snxr2", 0, 1, [.59, .481]],
                ["Zbbetngr", 66, 1, [.582, .45],
                    [.595,
                        .469
                    ],
                    [.609, .48]
                ],
                ["Yvirecbby Fgerrg", 68, 1, [.608, .45],
                    [.613, .483],
                    [.643, .481]
                ],
                ["snxr0", 0, 1, [.635, .45]],
                ["snxr0", 0, 1, [.639, .455]],
                ["snxr2", 0, 1, [.652, .489]],
                ["snxr2", 0, 1, [.656, .498]],
                ["Nyqtngr", 70, 1, [.639, .48],
                    [.626, .494],
                    [.656, .506]
                ]
            ]
        }, {
            c: "Zrgebcbyvgna",
            b: "zrgebcbyvgna_purfunz",
            a: "gsy-yh",
            f: [
                ["Purfunz", 0, 9, [.042, .049],
                    [.022, .051],
                    [.071, .138]
                ],
                ["snxr0", 0, 0, [.056, .065]],
                ["snxr2", 0, 0, [.071, .158]],
                ["snxr1", 0, 0, [.049, .072]],
                ["Punysbag & Yngvzre", 10, 8, [.077, .065],
                    [.06, .096],
                    [.077, .175]
                ]
            ]
        }, {
            c: "Zrgebcbyvgna",
            b: "zrgebcbyvgna_jngsbeq",
            a: "gsy-yh",
            f: [
                ["Jngsbeq", 0, 7, [.135, .07],
                    [.112, .103],
                    [.11, .168]
                ],
                ["snxr1", 0, 0, [.093, .118]],
                ["snxr1", 0, 0, [.091, .126]],
                ["Pebkyrl", 2, 7, [.135, .092],
                    [.091, .133],
                    [.11, .207]
                ],
                ["snxr0", 0, 0, [.135, .114]],
                ["snxr2", 0, 0, [.111, .215]],
                ["snxr1", 0, 0, [.09, .17]],
                ["Zbbe Cnex", 7, 67, [.141, .123],
                    [.092, .179],
                    [.117, .231]
                ]
            ]
        }, {
            c: "Zrgebcbyvgna",
            b: "zrgebcbyvgna_hkoevqtr",
            a: "gsy-yh",
            f: [
                ["Hkoevqtr", 0, 6, [.044, .165],
                    [.047, .323],
                    [.029, .326]
                ],
                ["Uvyyvatqba", 2, 6, [.065, .165],
                    [.061, .313],
                    [.046, .326]
                ],
                ["Vpxraunz",
                    4, 6, [.081, .165],
                    [.068, .308],
                    [.068, .323]
                ],
                ["Ehvfyvc", 7, 6, [.109, .165],
                    [.084, .295],
                    [.092, .325]
                ],
                ["snxr0", 0, 0, [.124, .165]],
                ["Ehvfyvc Znabe", 8, 6, [.129, .172],
                    [.092, .287],
                    [.11, .325]
                ],
                ["Rnfgpbgr", 10, 5, [.141, .19],
                    [.1, .281],
                    [.124, .325]
                ],
                ["Enlaref Ynar", 13, 5, [.151, .205],
                    [.109, .272],
                    [.14, .324]
                ],
                ["snxr0", 0, 0, [.16, .211]],
                ["Jrfg Uneebj", 15, 5, [.186, .211],
                    [.122, .262],
                    [.172, .325]
                ],
                ["snxr1", 0, 0, [.131, .257]],
                ["Uneebj-ba-gur-Uvyy", 19, 5, [.214, .211],
                    [.14, .256],
                    [.196, .324]
                ]
            ]
        }, {
            c: "Abegurea",
            b: "abegurea_znvayvar",
            a: "gsy-yh",
            f: [
                ["Zbeqra", 0, 4, [.348, .925],
                    [.375, .868],
                    [.419, .953]
                ],
                ["snxr1", 0, 0, [.375, .852]],
                ["snxr1", 0, 0, [.377, .845]],
                ["snxr1", 0, 0, [.38, .839]],
                ["Fbhgu Jvzoyrqba", 2, 34, [.377, .884],
                    [.385, .836],
                    [.431, .935]
                ],
                ["Pbyyvref Jbbq", 4, 3, [.386, .871],
                    [.402, .821],
                    [.443, .92]
                ],
                ["Gbbgvat Oebnqjnl", 6, 3, [.395, .858],
                    [.417, .81],
                    [.455, .902]
                ],
                ["Gbbgvat Orp", 8, 3, [.404, .845],
                    [.434, .794],
                    [.467, .885]
                ],
                ["Onyunz", 10, 3, [.412, .835],
                    [.448, .783],
                    [.478, .868]
                ],
                ["Pyncunz Fbhgu", 12, 23, [.422, .82],
                    [.471, .763],
                    [.491, .852]
                ],
                ["Pyncunz Pbzzba", 14, 2, [.431,
                        .808
                    ],
                    [.484, .752],
                    [.502, .837]
                ],
                ["Pyncunz Abegu", 16, 2, [.443, .791],
                    [.497, .741],
                    [.514, .818]
                ],
                ["Fgbpxjryy", 17, 2, [.457, .77],
                    [.529, .711],
                    [.531, .793]
                ],
                ["Biny", 19, 2, [.47, .752],
                    [.541, .684],
                    [.555, .764]
                ],
                ["Xraavatgba", 22, 2, [.478, .74],
                    [.55, .66],
                    [.572, .739]
                ],
                ["snxr2-ag", 0, 0, [.605, .694]],
                ["snxr2-ag", 0, 0, [.608, .684]],
                ["Ryrcunag & Pnfgyr", 24, 12, [.493, .72],
                    [.574, .601],
                    [.609, .676]
                ],
                ["Obebhtu", 25, 1, [.542, .649],
                    [.585, .574],
                    [.609, .647]
                ],
                ["snxr0-ag", 0, 1, [.582, .591]],
                ["Ybaqba Oevqtr", 27, 1, [.582, .579],
                    [.594, .549],
                    [.609,
                        .608
                    ]
                ],
                ["Onax", 29, 1, [.582, .505],
                    [.594, .496],
                    [.609, .532]
                ],
                ["Zbbetngr", 31, 1, [.582, .447],
                    [.594, .462],
                    [.609, .478]
                ],
                ["snxr2-ag", 0, 1, [.608, .439]],
                ["Byq Fgerrg", 32, 1, [.582, .394],
                    [.595, .426],
                    [.604, .421]
                ],
                ["snxr0-ag", 0, 1, [.582, .38]],
                ["snxr0-ag", 0, 1, [.577, .375]],
                ["snxr2-ag", 0, 1, [.598, .409]],
                ["snxr2-ag", 0, 1, [.592, .401]],
                ["snxr1-ag", 0, 1, [.593, .418]],
                ["snxr1-ag", 0, 1, [.59, .412]],
                ["Natry", 35, 1, [.556, .375],
                    [.559, .386],
                    [.585, .393]
                ],
                ["snxr2-ag", 0, 1, [.576, .389]],
                ["snxr1-ag", 0, 1, [.541, .371]],
                ["snxr1-ag", 0, 1, [.534, .37]],
                ["Xvat'f Pebff Fg. Cnapenf", 37, 1, [.522, .375],
                    [.503, .393],
                    [.533, .387]
                ],
                ["snxr0-ag", 0, 1, [.508, .375]],
                ["Rhfgba", 39, 1, [.502, .368],
                    [.481, .411],
                    [.503, .387]
                ],
                ["snxr0-ag", 0, 1, [.502, .329]],
                ["snxr0-ag", 0, 1, [.497, .321]],
                ["snxr2-ag", 0, 1, [.497, .386]],
                ["snxr2-ag", 0, 1, [.494, .378]],
                ["snxr2-ag", 0, 1, [.494, .326]],
                ["snxr2-ag", 0, 1, [.49, .32]],
                ["snxr1-ag", 0, 1, [.475, .416]],
                ["snxr1-ag", 0, 1, [.465, .393]],
                ["snxr1-ag", 0, 1, [.465, .366]],
                ["snxr1-ag", 0, 1, [.464, .358]],
                ["Pnzqra Gbja", 43, 2, [.497, .314],
                    [.46, .348],
                    [.484, .316]
                ],
                ["snxr0",
                    0, 0, [.497, .302]
                ],
                ["snxr2", 0, 0, [.479, .313]],
                ["snxr2", 0, 0, [.475, .302]],
                ["snxr2", 0, 0, [.474, .276]],
                ["snxr1", 0, 0, [.458, .339]],
                ["snxr1", 0, 0, [.456, .332]],
                ["Xragvfu Gbja", 44, 2, [.524, .265],
                    [.456, .285],
                    [.469, .263]
                ],
                ["Ghsaryy Cnex", 46, 2, [.529, .249],
                    [.456, .262],
                    [.461, .253]
                ],
                ["Nepujnl", 48, 23, [.529, .213],
                    [.456, .226],
                    [.448, .233]
                ],
                ["snxr1", 0, 0, [.454, .22]],
                ["snxr1", 0, 0, [.451, .214]],
                ["Uvtutngr", 51, 3, [.529, .194],
                    [.431, .198],
                    [.436, .217]
                ],
                ["Rnfg Svapuyrl", 53, 3, [.529, .176],
                    [.41, .18],
                    [.424, .201]
                ],
                ["Svapuyrl Prageny", 57,
                    4, [.529, .156],
                    [.388, .162],
                    [.412, .184]
                ],
                ["snxr1", 0, 0, [.378, .153]],
                ["snxr1", 0, 0, [.375, .145]],
                ["Jrfg Svapuyrl", 60, 4, [.529, .127],
                    [.375, .133],
                    [.396, .163]
                ],
                ["Jbbqfvqr Cnex", 61, 4, [.529, .11],
                    [.375, .115],
                    [.384, .145]
                ],
                ["Gbggrevqtr & Jurgfgbar", 64, 4, [.529, .093],
                    [.375, .097],
                    [.372, .129]
                ],
                ["Uvtu Onearg", 67, 5, [.529, .075],
                    [.375, .08],
                    [.361, .113]
                ]
            ]
        }, {
            c: "Abegurea",
            b: "abegurea_rqtjner",
            a: "gsy-yh",
            f: [
                ["Xraavatgba", 0, 2, [.478, .74],
                    [.55, .66],
                    [.572, .739]
                ],
                ["snxr0", 0, 1, [.48, .736]],
                ["snxr2", 0, 1, [.578, .721]],
                ["snxr2", 0, 1, [.575,
                    .705
                ]],
                ["snxr1", 0, 1, [.553, .653]],
                ["snxr1", 0, 1, [.554, .644]],
                ["snxr1", 0, 1, [.553, .584]],
                ["snxr1", 0, 1, [.551, .581]],
                ["Jngreybb", 2, 1, [.48, .611],
                    [.545, .574],
                    [.555, .654]
                ],
                ["Rzonaxzrag", 3, 1, [.48, .567],
                    [.525, .556],
                    [.539, .616]
                ],
                ["Punevat Pebff", 4, 1, [.48, .539],
                    [.517, .551],
                    [.533, .602]
                ],
                ["snxr2", 0, 1, [.531, .591]],
                ["snxr2", 0, 1, [.531, .573]],
                ["snxr1", 0, 1, [.513, .546]],
                ["Yrvprfgre Fdhner", 6, 1, [.48, .497],
                    [.5, .52],
                    [.52, .547]
                ],
                ["Gbggraunz Pbheg Ebnq", 7, 1, [.48, .46],
                    [.49, .492],
                    [.501, .501]
                ],
                ["snxr2", 0, 1, [.481, .451]],
                ["Tbbqtr Fgerrg",
                    9, 1, [.48, .441],
                    [.48, .467],
                    [.482, .442]
                ],
                ["Jneera Fgerrg", 10, 1, [.48, .401],
                    [.472, .448],
                    [.48, .412]
                ],
                ["snxr0", 0, 1, [.481, .392]],
                ["snxr0", 0, 1, [.491, .374]],
                ["snxr1", 0, 1, [.467, .432]],
                ["snxr1", 0, 1, [.468, .421]],
                ["snxr1", 0, 1, [.472, .415]],
                ["Rhfgba", 11, 1, [.492, .368],
                    [.48, .408],
                    [.5, .383]
                ],
                ["snxr1", 0, 1, [.484, .402]],
                ["snxr1", 0, 1, [.482, .391]],
                ["Zbeavatgba Perfprag", 14, 2, [.492, .338],
                    [.476, .375],
                    [.5, .353]
                ],
                ["snxr0", 0, 0, [.492, .327]],
                ["snxr0", 0, 0, [.497, .321]],
                ["snxr2", 0, 0, [.498, .332]],
                ["snxr2", 0, 0, [.494, .321]],
                ["snxr1",
                    0, 0, [.465, .359]
                ],
                ["Pnzqra Gbja", 16, 2, [.497, .314],
                    [.46, .348],
                    [.483, .315]
                ],
                ["snxr0", 0, 0, [.497, .302]],
                ["snxr1", 0, 0, [.456, .341]],
                ["Punyx Snez", 17, 2, [.487, .29],
                    [.433, .322],
                    [.455, .318]
                ],
                ["snxr1", 0, 0, [.417, .308]],
                ["Oryfvmr Cnex", 19, 2, [.473, .271],
                    [.412, .3],
                    [.423, .316]
                ],
                ["snxr2", 0, 0, [.385, .315]],
                ["Unzcfgrnq", 21, 23, [.45, .237],
                    [.393, .253],
                    [.374, .302]
                ],
                ["snxr1", 0, 0, [.384, .228]],
                ["Tbyqref Terra", 26, 3, [.438, .22],
                    [.376, .22],
                    [.362, .285]
                ],
                ["Oerag Pebff", 29, 3, [.428, .206],
                    [.355, .203],
                    [.35, .268]
                ],
                ["Uraqba Prageny", 30,
                    34, [.415, .188],
                    [.336, .186],
                    [.338, .252]
                ],
                ["Pbyvaqnyr", 33, 4, [.405, .174],
                    [.315, .169],
                    [.326, .235]
                ],
                ["Oheag Bnx", 35, 4, [.394, .158],
                    [.294, .151],
                    [.313, .218]
                ],
                ["Rqtjner", 38, 5, [.377, .134],
                    [.27, .13],
                    [.302, .201]
                ]
            ]
        }, {
            c: "Abegurea",
            b: "abegurea_zvyyuvyyrnfg",
            a: "gsy-yh",
            f: [
                ["Zvyy Uvyy Rnfg", 0, 4, [.513, .12],
                    [.358, .139],
                    [.385, .175]
                ],
                ["snxr0", 0, 0, [.529, .141]],
                ["snxr2", 0, 0, [.403, .175]],
                ["Svapuyrl Prageny", 2, 4, [.529, .156],
                    [.388, .163],
                    [.412, .185]
                ]
            ]
        }, {
            c: "Cvppnqvyyl",
            b: "cvppnqvyyl_znvayvar",
            a: "gsy-yh",
            f: [
                ["Hkoevqtr", 0, 6, [.044,
                        .167
                    ],
                    [.049, .329],
                    [.03, .33]
                ],
                ["Uvyyvatqba", 2, 6, [.065, .167],
                    [.062, .319],
                    [.046, .33]
                ],
                ["Vpxraunz", 4, 6, [.081, .167],
                    [.069, .311],
                    [.068, .329]
                ],
                ["Ehvfyvc", 7, 6, [.109, .167],
                    [.086, .298],
                    [.093, .33]
                ],
                ["snxr0", 0, 0, [.123, .167]],
                ["Ehvfyvc Znabe", 8, 6, [.128, .175],
                    [.093, .291],
                    [.109, .33]
                ],
                ["Rnfgpbgr", 10, 5, [.14, .192],
                    [.101, .285],
                    [.124, .331]
                ],
                ["Enlaref Ynar", 13, 5, [.151, .205],
                    [.11, .278],
                    [.14, .33]
                ],
                ["snxr0", 0, 0, [.166, .227]],
                ["snxr2", 0, 0, [.149, .331]],
                ["snxr2", 0, 0, [.172, .364]],
                ["snxr1", 0, 0, [.118, .272]],
                ["snxr1", 0, 0, [.127,
                    .274
                ]],
                ["snxr1", 0, 0, [.134, .281]],
                ["Fbhgu Uneebj", 17, 5, [.166, .244],
                    [.136, .289],
                    [.174, .381]
                ],
                ["Fhqohel Uvyy", 20, 4, [.166, .275],
                    [.149, .32],
                    [.174, .426]
                ],
                ["Fhqohel Gbja", 22, 4, [.166, .306],
                    [.159, .345],
                    [.174, .471]
                ],
                ["Nycregba", 25, 4, [.166, .338],
                    [.168, .372],
                    [.174, .514]
                ],
                ["snxr1", 0, 0, [.171, .379]],
                ["Cnex Eblny", 27, 3, [.166, .44],
                    [.172, .421],
                    [.174, .582]
                ],
                ["Abegu Rnyvat", 29, 3, [.166, .462],
                    [.172, .486],
                    [.174, .604]
                ],
                ["snxr2", 0, 0, [.175, .63]],
                ["Rnyvat Pbzzba", 31, 3, [.167, .533],
                    [.171, .534],
                    [.186, .65]
                ],
                ["snxr0", 0, 0, [.167,
                    .559
                ]],
                ["snxr1", 0, 0, [.173, .551]],
                ["Npgba Gbja", 36, 3, [.17, .568],
                    [.179, .566],
                    [.201, .673]
                ],
                ["snxr2", 0, 0, [.25, .739]],
                ["snxr1", 0, 0, [.197, .608]],
                ["Unzzrefzvgu", 42, 2, [.271, .568],
                    [.262, .607],
                    [.274, .738]
                ],
                ["snxr1", 0, 0, [.273, .611]],
                ["Onebaf Pbheg", 44, 2, [.287, .568],
                    [.287, .622],
                    [.302, .741]
                ],
                ["snxr1", 0, 0, [.3, .632]],
                ["snxr1", 0, 0, [.309, .635]],
                ["snxr1", 0, 0, [.329, .636]],
                ["snxr1", 0, 0, [.338, .63]],
                ["Rney'f Pbheg", 47, 12, [.322, .568],
                    [.346, .623],
                    [.367, .737]
                ],
                ["snxr1", 0, 1, [.36, .613]],
                ["Tybhprfgre Ebnq", 49, 1, [.347, .568],
                    [.375, .613],
                    [.426, .738]
                ],
                ["snxr2", 0, 1, [.434, .738]],
                ["snxr1", 0, 1, [.391, .612]],
                ["Fbhgu Xrafvatgba", 50, 1, [.362, .568],
                    [.397, .606],
                    [.45, .727]
                ],
                ["snxr0", 0, 1, [.367, .568]],
                ["snxr2", 0, 1, [.46, .716]],
                ["snxr2", 0, 1, [.464, .699]],
                ["Xavtugfoevqtr", 53, 1, [.385, .545],
                    [.427, .583],
                    [.465, .674]
                ],
                ["Ulqr Cnex Pbeare", 54, 1, [.406, .516],
                    [.443, .569],
                    [.465, .645]
                ],
                ["snxr0", 0, 1, [.41, .507]],
                ["snxr2", 0, 1, [.466, .623]],
                ["Terra Cnex", 57, 1, [.417, .507],
                    [.473, .544],
                    [.479, .603]
                ],
                ["Cvppnqvyyl Pvephf", 58, 1, [.458, .507],
                    [.489, .531],
                    [.498, .579]
                ],
                ["snxr0", 0, 1, [.471, .507]],
                ["Yrvprfgre Fdhner", 60, 1, [.48, .497],
                    [.501, .521],
                    [.52, .547]
                ],
                ["Pbirag Tneqra", 61, 1, [.492, .48],
                    [.513, .511],
                    [.531, .533]
                ],
                ["snxr2", 0, 1, [.534, .526]],
                ["snxr1", 0, 1, [.519, .505]],
                ["snxr1", 0, 1, [.523, .497]],
                ["Ubyobea", 63, 1, [.505, .46],
                    [.523, .484],
                    [.535, .492]
                ],
                ["snxr0", 0, 1, [.516, .443]],
                ["Ehffryy Fdhner", 65, 1, [.516, .424],
                    [.513, .454],
                    [.535, .446]
                ],
                ["snxr0", 0, 1, [.516, .394]],
                ["snxr1", 0, 1, [.496, .411]],
                ["snxr1", 0, 1, [.497, .401]],
                ["snxr1", 0, 1, [.5, .396]],
                ["Xvat'f Pebff Fg. Cnapenf", 67, 1, [.522,
                        .387
                    ],
                    [.503, .392],
                    [.534, .395]
                ],
                ["snxr2", 0, 1, [.535, .312]],
                ["snxr2", 0, 1, [.537, .305]],
                ["snxr1", 0, 1, [.51, .383]],
                ["snxr1", 0, 1, [.514, .368]],
                ["snxr1", 0, 1, [.514, .331]],
                ["Pnyrqbavna Ebnq", 70, 2, [.589, .292],
                    [.527, .299],
                    [.541, .299]
                ],
                ["Ubyybjnl Ebnq", 72, 2, [.602, .273],
                    [.539, .267],
                    [.552, .283]
                ],
                ["Nefrany", 73, 2, [.613, .257],
                    [.549, .243],
                    [.564, .267]
                ],
                ["snxr2", 0, 0, [.57, .257]],
                ["Svafohel Cnex", 75, 2, [.623, .242],
                    [.556, .222],
                    [.571, .243]
                ],
                ["snxr0", 0, 0, [.63, .23]],
                ["Znabe Ubhfr", 77, 23, [.63, .213],
                    [.563, .207],
                    [.57, .226]
                ],
                ["snxr2",
                    0, 0, [.571, .197]
                ],
                ["snxr2", 0, 0, [.566, .185]],
                ["snxr1", 0, 0, [.565, .194]],
                ["snxr1", 0, 0, [.563, .18]],
                ["snxr1", 0, 0, [.558, .17]],
                ["Gheacvxr Ynar", 80, 3, [.63, .177],
                    [.554, .165],
                    [.552, .164]
                ],
                ["Jbbq Terra", 82, 3, [.63, .16],
                    [.538, .151],
                    [.54, .146]
                ],
                ["Obhaqf Terra", 85, 34, [.63, .143],
                    [.514, .131],
                    [.528, .128]
                ],
                ["Neabf Tebir", 89, 4, [.63, .125],
                    [.495, .116],
                    [.516, .114]
                ],
                ["Fbhgutngr", 92, 4, [.63, .108],
                    [.481, .104],
                    [.504, .098]
                ],
                ["Bnxjbbq", 95, 5, [.63, .091],
                    [.463, .089],
                    [.492, .08]
                ],
                ["Pbpxsbfgref", 99, 5, [.63, .074],
                    [.451, .079],
                    [.481, .064]
                ]
            ]
        },
        {
            c: "Cvppnqvyyl",
            b: "cvppnqvyyl_fbhgu",
            a: "gsy-yh",
            f: [
                ["Urnguebj Grezvany 5", 0, 6, [.029, .734],
                    [.068, .604],
                    [.034, .815]
                ],
                ["snxr1", 0, 0, [.082, .605]],
                ["Urnguebj Grezvanyf 1-2-3", 3, 6, [.058, .694],
                    [.086, .606],
                    [.059, .814]
                ],
                ["snxr2", 0, 0, [.079, .812]],
                ["snxr1", 0, 0, [.098, .616]],
                ["Unggba Pebff", 6, 56, [.071, .673],
                    [.105, .614],
                    [.085, .807]
                ],
                ["Ubhafybj Jrfg", 10, 5, [.085, .656],
                    [.118, .603],
                    [.098, .791]
                ],
                ["Ubhafybj Prageny", 12, 4, [.098, .636],
                    [.125, .597],
                    [.109, .773]
                ],
                ["Ubhafybj Rnfg", 14, 4, [.108, .622],
                    [.131, .592],
                    [.122, .756]
                ],
                ["Bfgreyrl",
                    16, 4, [.116, .611],
                    [.138, .586],
                    [.133, .741]
                ],
                ["Obfgba Znabe", 19, 4, [.126, .597],
                    [.149, .579],
                    [.146, .724]
                ],
                ["Abegusvryqf", 22, 3, [.136, .583],
                    [.157, .57],
                    [.157, .707]
                ],
                ["snxr0", 0, 0, [.145, .568]],
                ["Fbhgu Rnyvat", 23, 3, [.152, .568],
                    [.164, .566],
                    [.168, .692]
                ],
                ["snxr2", 0, 0, [.182, .676]],
                ["snxr2", 0, 0, [.192, .673]],
                ["snxr1", 0, 0, [.168, .562]],
                ["snxr1", 0, 0, [.173, .562]],
                ["Npgba Gbja", 27, 3, [.17, .568],
                    [.179, .568],
                    [.202, .673]
                ]
            ]
        }, {
            c: "Cvppnqvyyl",
            b: "cvppnqvyyl_urnguebj",
            a: "gsy-yh",
            f: [
                ["Unggba Pebff", 0, 6, [.071, .673],
                    [.105, .614],
                    [.085,
                        .807
                    ]
                ],
                ["snxr0", 0, 0, [.068, .678]],
                ["snxr0", 0, 0, [.068, .717]],
                ["snxr0", 0, 0, [.058, .726]],
                ["Urnguebj Grezvany 4", 2, 6, [.058, .726],
                    [.08, .622],
                    [.063, .837]
                ],
                ["snxr0", 0, 0, [.05, .718]],
                ["snxr0", 0, 0, [.048, .71]],
                ["snxr2", 0, 0, [.054, .841]],
                ["snxr2", 0, 0, [.048, .835]],
                ["snxr2", 0, 0, [.048, .824]],
                ["Urnguebj Grezvanyf 1-2-3", 8, 6, [.058, .694],
                    [.087, .605],
                    [.058, .814]
                ]
            ]
        }, {
            c: "Ivpgbevn",
            b: "ivpgbevn_znvayvar",
            a: "gsy-yh",
            f: [
                ["Oevkgba", 0, 2, [.481, .804],
                    [.542, .738],
                    [.542, .82]
                ],
                ["snxr1", 0, 0, [.534, .735]],
                ["snxr1", 0, 0, [.53, .727]],
                ["snxr1",
                    0, 0, [.53, .723]
                ],
                ["Fgbpxjryy", 2, 2, [.457, .77],
                    [.529, .711],
                    [.531, .794]
                ],
                ["snxr1", 0, 0, [.529, .692]],
                ["snxr1", 0, 0, [.526, .682]],
                ["Inhkunyy", 4, 12, [.423, .722],
                    [.506, .664],
                    [.512, .747]
                ],
                ["snxr0", 0, 1, [.417, .71]],
                ["Cvzyvpb", 5, 1, [.417, .654],
                    [.484, .647],
                    [.5, .717]
                ],
                ["snxr1", 0, 1, [.476, .639]],
                ["snxr1", 0, 1, [.473, .633]],
                ["snxr1", 0, 1, [.473, .628]],
                ["Ivpgbevn", 7, 1, [.417, .577],
                    [.473, .606],
                    [.488, .688]
                ],
                ["snxr2", 0, 1, [.479, .659]],
                ["Terra Cnex", 9, 1, [.417, .507],
                    [.473, .543],
                    [.48, .605]
                ],
                ["snxr0", 0, 1, [.417, .489]],
                ["Bksbeq Pvephf",
                    11, 1, [.438, .46],
                    [.471, .496],
                    [.479, .529]
                ],
                ["Jneera Fgerrg", 13, 1, [.48, .401],
                    [.472, .448],
                    [.479, .412]
                ],
                ["snxr2", 0, 1, [.482, .396]],
                ["snxr2", 0, 1, [.491, .384]],
                ["snxr1", 0, 1, [.473, .432]],
                ["snxr1", 0, 1, [.476, .423]],
                ["snxr1", 0, 1, [.479, .419]],
                ["Rhfgba", 14, 1, [.502, .368],
                    [.482, .415],
                    [.502, .384]
                ],
                ["snxr0", 0, 1, [.508, .364]],
                ["Xvat'f Pebff Fg. Cnapenf", 16, 1, [.522, .364],
                    [.504, .396],
                    [.534, .384]
                ],
                ["snxr0", 0, 1, [.588, .364]],
                ["snxr0", 0, 1, [.608, .336]],
                ["snxr2", 0, 1, [.571, .383]],
                ["snxr2", 0, 1, [.58, .376]],
                ["snxr2", 0, 1, [.586, .363]],
                ["snxr1", 0, 1, [.541, .366]],
                ["snxr1", 0, 1, [.545, .359]],
                ["snxr1", 0, 1, [.546, .354]],
                ["Uvtuohel & Vfyvatgba", 19, 2, [.608, .295],
                    [.545, .335],
                    [.585, .33]
                ],
                ["snxr0", 0, 0, [.608, .277]],
                ["snxr2", 0, 0, [.585, .279]],
                ["snxr2", 0, 0, [.576, .255]],
                ["snxr1", 0, 0, [.546, .266]],
                ["Svafohel Cnex", 21, 2, [.628, .249],
                    [.56, .225],
                    [.574, .242]
                ],
                ["snxr0", 0, 0, [.652, .214]],
                ["snxr2", 0, 0, [.576, .205]],
                ["snxr2", 0, 0, [.578, .197]],
                ["snxr2", 0, 0, [.582, .191]],
                ["snxr1", 0, 0, [.578, .186]],
                ["Frira Fvfgref", 25, 3, [.662, .214],
                    [.595, .184],
                    [.588, .192]
                ],
                ["Gbggraunz Unyr",
                    27, 3, [.682, .214],
                    [.639, .183],
                    [.629, .191]
                ],
                ["Oynpxubefr Ebnq", 29, 3, [.711, .214],
                    [.676, .184],
                    [.658, .193]
                ],
                ["Jnygunzfgbj Prageny", 30, 3, [.741, .214],
                    [.725, .184],
                    [.69, .19]
                ]
            ]
        }, {
            c: "JngreybbPvgl",
            b: "jngreybb_znvayvar",
            a: "gsy-yh",
            f: [
                ["Jngreybb", 0, 1, [.473, .611],
                    [.543, .572],
                    [.553, .656]
                ],
                ["snxr0", 0, 1, [.541, .611]],
                ["snxr0", 0, 1, [.572, .568]],
                ["snxr2", 0, 1, [.555, .612]],
                ["snxr2", 0, 1, [.6, .548]],
                ["snxr2", 0, 1, [.604, .534]],
                ["snxr1", 0, 1, [.558, .559]],
                ["snxr1", 0, 1, [.563, .55]],
                ["snxr1", 0, 1, [.562, .51]],
                ["snxr1", 0, 1, [.566, .502]],
                ["snxr1", 0, 1, [.574, .498]],
                ["Onax", 4, 1, [.572, .49],
                    [.594, .497],
                    [.607, .523]
                ]
            ]
        }, {
            c: "Biretebhaq",
            b: "biretebhaq_peblqba",
            a: "gsy-yh",
            f: [
                ["Uvtuohel & Vfyvatgba", 0, 2, [.621, .313],
                    [.545, .335],
                    [.585, .333]
                ],
                ["Pnabaohel", 2, 2, [.639, .313],
                    [.582, .335],
                    [.62, .333]
                ],
                ["snxr0", 0, 0, [.654, .313]],
                ["snxr2", 0, 0, [.631, .333]],
                ["snxr2", 0, 0, [.637, .342]],
                ["snxr1", 0, 0, [.608, .335]],
                ["snxr1", 0, 0, [.613, .337]],
                ["Qnyfgba Whapgvba", 5, 2, [.666, .33],
                    [.615, .339],
                    [.643, .358]
                ],
                ["snxr0", 0, 0, [.671, .338]],
                ["snxr1", 0, 0, [.619, .346]],
                ["Unttrefgba",
                    6, 2, [.671, .35863],
                    [.619, .368],
                    [.653, .381]
                ],
                ["snxr1", 0, 0, [.619, .388]],
                ["Ubkgba", 8, 12, [.671, .37807],
                    [.62, .395],
                    [.66, .397]
                ],
                ["Fuberqvgpu Uvtu Fgerrg", 11, 1, [.671, .415],
                    [.634, .429],
                    [.667, .416]
                ],
                ["snxr2", 0, 0, [.695, .483]],
                ["Juvgrpuncry", 13, 2, [.671, .442],
                    [.652, .472],
                    [.696, .501]
                ],
                ["Funqjryy", 15, 2, [.671, .521],
                    [.663, .502],
                    [.697, .539]
                ],
                ["Jnccvat", 17, 2, [.671, .54],
                    [.676, .536],
                    [.697, .568]
                ],
                ["Ebgureuvgur", 19, 2, [.671, .564],
                    [.689, .568],
                    [.697, .59]
                ],
                ["Pnanqn Jngre", 21, 2, [.671, .579],
                    [.696, .586],
                    [.697, .607]
                ],
                ["Fheerl Dhnlf",
                    22, 2, [.671, .642],
                    [.705, .61],
                    [.697, .647]
                ],
                ["snxr1", 0, 0, [.715, .634]],
                ["snxr1", 0, 0, [.716, .642]],
                ["Arj Pebff Tngr", 27, 2, [.671, .716],
                    [.716, .697],
                    [.697, .737]
                ],
                ["Oebpxyrl", 29, 2, [.671, .737],
                    [.717, .725],
                    [.697, .761]
                ],
                ["snxr2", 0, 0, [.697, .773]],
                ["Ubabe Bnx Cnex", 32, 3, [.671, .755],
                    [.716, .767],
                    [.691, .788]
                ],
                ["snxr1", 0, 0, [.714, .776]],
                ["Sberfg Uvyy", 35, 3, [.671, .773],
                    [.702, .787],
                    [.676, .807]
                ],
                ["Flqraunz", 37, 3, [.671, .79],
                    [.685, .802],
                    [.658, .83]
                ],
                ["Cratr Jrfg", 40, 4, [.671, .808],
                    [.643, .837],
                    [.627, .873]
                ],
                ["Nareyrl", 42, 4, [.671,
                        .825
                    ],
                    [.625, .852],
                    [.611, .897]
                ],
                ["Abejbbq Whapgvba", 45, 4, [.671, .841],
                    [.603, .871],
                    [.594, .919]
                ],
                ["Jrfg Peblqba", 52, 5, [.671, .875],
                    [.574, .894],
                    [.578, .941]
                ]
            ]
        }, {
            c: "Biretebhaq",
            b: "biretebhaq_pyncunz_fgengsbeq",
            a: "gsy-yh",
            f: [
                ["Pyncunz Whapgvba", 0, 2, [.358, .738],
                    [.415, .738],
                    [.416, .856]
                ],
                ["snxr0", 0, 0, [.358, .709]],
                ["snxr2", 0, 0, [.398, .834]],
                ["Vzcrevny Junes", 4, 2, [.341, .686],
                    [.37, .7],
                    [.375, .833]
                ],
                ["snxr0", 0, 0, [.308, .638]],
                ["snxr2", 0, 0, [.366, .828]],
                ["snxr2", 0, 0, [.352, .806]],
                ["snxr2", 0, 0, [.35, .797]],
                ["snxr1", 0, 0, [.349, .681]],
                ["Jrfg Oebzcgba", 7, 2, [.308, .621],
                    [.338, .654],
                    [.35, .787]
                ],
                ["Xrafvatgba (Bylzcvn)", 10, 2, [.308, .535],
                    [.311, .585],
                    [.35, .713]
                ],
                ["snxr0", 0, 0, [.308, .518]],
                ["snxr2", 0, 0, [.348, .701]],
                ["snxr2", 0, 0, [.346, .692]],
                ["Furcureq'f Ohfu", 12, 2, [.293, .5],
                    [.293, .542],
                    [.31, .642]
                ],
                ["snxr0", 0, 0, [.222, .399]],
                ["snxr0", 0, 0, [.222, .319]],
                ["snxr0", 0, 0, [.229, .31]],
                ["snxr2", 0, 0, [.277, .597]],
                ["snxr2", 0, 0, [.274, .587]],
                ["snxr2", 0, 0, [.274, .492]],
                ["snxr2", 0, 0, [.277, .481]],
                ["snxr1", 0, 0, [.253, .438]],
                ["Jvyyrfqra Whapgvba", 20,
                    23, [.259, .31],
                    [.252, .395],
                    [.299, .45]
                ],
                ["snxr1", 0, 0, [.256, .385]],
                ["Xrafny Evfr", 22, 2, [.281, .31],
                    [.29, .357],
                    [.319, .425]
                ],
                ["Oebaqrfohel Cnex", 24, 2, [.312, .31],
                    [.315, .336],
                    [.332, .405]
                ],
                ["Oebaqrfohel", 26, 2, [.339, .31],
                    [.338, .317],
                    [.346, .387]
                ],
                ["snxr2", 0, 0, [.36, .368]],
                ["Jrfg Unzcfgrnq", 28, 2, [.38, .31],
                    [.366, .292],
                    [.384, .368]
                ],
                ["snxr0", 0, 0, [.416, .31]],
                ["snxr2", 0, 0, [.391, .366]],
                ["Svapuyrl Ebnq & Sebtany", 29, 2, [.435, .283],
                    [.382, .282],
                    [.397, .356]
                ],
                ["snxr0", 0, 0, [.458, .248]],
                ["snxr2", 0, 0, [.407, .345]],
                ["snxr2", 0,
                    0, [.41, .333]
                ],
                ["snxr1", 0, 0, [.386, .278]],
                ["Unzcfgrnq Urngu", 32, 2, [.47, .248],
                    [.415, .279],
                    [.41, .308]
                ],
                ["snxr2", 0, 0, [.411, .293]],
                ["snxr2", 0, 0, [.415, .281]],
                ["snxr2", 0, 0, [.42, .275]],
                ["Tbfcry Bnx", 34, 2, [.495, .248],
                    [.432, .28],
                    [.43, .275]
                ],
                ["snxr0", 0, 0, [.503, .248]],
                ["snxr1", 0, 0, [.438, .28]],
                ["snxr1", 0, 0, [.443, .284]],
                ["Xragvfu Gbja Jrfg", 36, 2, [.511, .259],
                    [.451, .301],
                    [.461, .275]
                ],
                ["snxr2", 0, 0, [.471, .277]],
                ["snxr1", 0, 0, [.459, .319]],
                ["snxr1", 0, 0, [.465, .325]],
                ["Pnzqra Ebnq", 40, 2, [.533, .292],
                    [.476, .326],
                    [.492, .304]
                ],
                ["snxr0", 0, 0, [.541, .303]],
                ["snxr2", 0, 0, [.508, .325]],
                ["Pnyrqbavna Ebnq & Oneafohel", 43, 2, [.594, .303],
                    [.526, .327],
                    [.545, .326]
                ],
                ["Uvtuohel & Vfyvatgba", 46, 2, [.614, .303],
                    [.545, .326],
                    [.586, .325]
                ],
                ["Pnabaohel", 48, 2, [.639, .303],
                    [.582, .326],
                    [.62, .325]
                ],
                ["Qnyfgba Xvatfynaq", 50, 2, [.656, .303],
                    [.615, .326],
                    [.651, .326]
                ],
                ["snxr0", 0, 0, [.662, .304]],
                ["snxr0", 0, 0, [.678, .326]],
                ["Unpxarl Prageny", 52, 2, [.706, .326],
                    [.648, .327],
                    [.68, .325]
                ],
                ["Ubzregba", 54, 2, [.733, .326],
                    [.68, .327],
                    [.721, .326]
                ],
                ["snxr1", 0, 0, [.711, .327]],
                ["Unpxarl Jvpx",
                    56, 2, [.761, .326],
                    [.724, .338],
                    [.752, .325]
                ],
                ["snxr2", 0, 0, [.794, .325]],
                ["snxr1", 0, 0, [.736, .347]],
                ["Fgengsbeq", 62, 23, [.786, .326],
                    [.772, .346],
                    [.806, .342]
                ]
            ]
        }, {
            c: "Biretebhaq",
            b: "biretebhaq_jngsbeq_whapgvba",
            a: "gsy-rhf",
            f: [
                ["Jngsbeq Whapgvba", 0, "W", [.259, .044],
                    [.139, .079],
                    [.234, .122]
                ],
                ["Jngsbeq Uvtu Fgerrg", 3, 8, [.259, .065],
                    [.14, .108],
                    [.235, .145]
                ],
                ["Ohfurl", 7, 8, [.259, .087],
                    [.14, .133],
                    [.235, .178]
                ],
                ["snxr1", 0, 0, [.14, .145]],
                ["Pnecraqref Cnex", 8, 7, [.259, .106],
                    [.14, .15],
                    [.235, .206]
                ],
                ["Ungpu Raq", 11, 6, [.259, .125],
                    [.149, .173],
                    [.234, .232]
                ],
                ["Urnqfgbar Ynar", 13, 5, [.259, .145],
                    [.155, .188],
                    [.234, .259]
                ],
                ["Uneebj & Jrnyqfgbar", 16, 5, [.259, .161],
                    [.162, .206],
                    [.234, .287]
                ],
                ["Xragba", 18, 4, [.259, .183],
                    [.176, .239],
                    [.234, .311]
                ],
                ["snxr2", 0, 0, [.235, .362]],
                ["Fbhgu Xragba", 20, 4, [.259, .237],
                    [.19, .274],
                    [.241, .371]
                ],
                ["Abegu Jrzoyrl", 22, 4, [.259, .251],
                    [.198, .296],
                    [.252, .386]
                ],
                ["Jrzoyrl Prageny", 24, 4, [.259, .264],
                    [.208, .32],
                    [.264, .4]
                ],
                ["Fgbaroevqtr Cnex", 27, 3, [.259, .277],
                    [.219, .345],
                    [.272, .414]
                ],
                ["Uneyrfqra", 29, 3, [.259, .29],
                    [.229, .372],
                    [.283, .431]
                ],
                ["snxr1", 0, 0, [.238, .392]],
                ["Jvyyrfqra Whapgvba", 31, 23, [.259, .31],
                    [.253, .392],
                    [.298, .45]
                ],
                ["Xrafny Terra", 34, 2, [.259, .326],
                    [.286, .394],
                    [.31, .468]
                ],
                ["Dhrra'f Cnex", 36, 2, [.259, .339],
                    [.318, .393],
                    [.323, .486]
                ],
                ["snxr0", 0, 0, [.259, .348]],
                ["snxr0", 0, 0, [.264, .354]],
                ["snxr2", 0, 0, [.338, .502]],
                ["snxr2", 0, 0, [.346, .501]],
                ["snxr1", 0, 0, [.329, .392]],
                ["Xvyohea Uvtu Ebnq", 38, 2, [.311, .354],
                    [.345, .38],
                    [.364, .479]
                ],
                ["snxr1", 0, 0, [.361, .367]],
                ["Fbhgu Unzcfgrnq", 40, 2, [.35, .354],
                    [.382, .368],
                    [.389, .445]
                ],
                ["snxr0",
                    0, 1, [.474, .354]
                ],
                ["snxr2", 0, 1, [.438, .377]],
                ["snxr1", 0, 1, [.459, .368]],
                ["snxr1", 0, 1, [.467, .376]],
                ["Rhfgba", 51, 1, [.483, .369],
                    [.478, .404],
                    [.488, .376]
                ]
            ]
        }, {
            c: "Biretebhaq",
            b: "biretebhaq_evpuzbaq",
            a: "gsy-yh",
            f: [
                ["Evpuzbaq", 0, 4, [.188, .675],
                    [.136, .732],
                    [.164, .771]
                ],
                ["Xrj Tneqraf", 3, 34, [.188, .654],
                    [.163, .668],
                    [.18, .748]
                ],
                ["Thaarefohel", 6, 3, [.188, .614],
                    [.18, .623],
                    [.198, .722]
                ],
                ["snxr0", 0, 0, [.188, .584]],
                ["snxr0", 0, 0, [.208, .553]],
                ["Fbhgu Npgba", 9, 3, [.208, .543],
                    [.202, .57],
                    [.225, .687]
                ],
                ["Npgba Prageny", 12, 3, [.208,
                        .523
                    ],
                    [.216, .536],
                    [.235, .672]
                ],
                ["snxr0", 0, 0, [.208, .34]],
                ["snxr0", 0, 0, [.229, .31]],
                ["snxr2", 0, 0, [.241, .659]],
                ["snxr2", 0, 0, [.243, .65]],
                ["snxr2", 0, 0, [.243, .537]],
                ["snxr2", 0, 0, [.247, .523]],
                ["snxr1", 0, 0, [.253, .44]],
                ["Jvyyrfqra Whapgvba", 18, 23, [.259, .31],
                    [.252, .393],
                    [.299, .451]
                ]
            ]
        }, {
            c: "Biretebhaq",
            b: "biretebhaq_arj_pebff",
            a: "gsy-yh",
            f: [
                ["Fheerl Dhnlf", 0, 2, [.671, .642],
                    [.706, .611],
                    [.697, .647]
                ],
                ["snxr0", 0, 0, [.671, .654]],
                ["snxr2", 0, 0, [.697, .687]],
                ["snxr1", 0, 0, [.715, .635]],
                ["snxr1", 0, 0, [.717, .658]],
                ["Arj Pebff",
                    6, 2, [.713, .715],
                    [.729, .692],
                    [.714, .71]
                ]
            ]
        }, {
            c: "Biretebhaq",
            b: "biretebhaq_pelfgny_cnynpr",
            a: "gsy-yh",
            f: [
                ["Flqraunz", 0, 3, [.671, .79],
                    [.685, .802],
                    [.659, .829]
                ],
                ["snxr0", 0, 0, [.671, .798]],
                ["snxr2", 0, 0, [.642, .852]],
                ["snxr1", 0, 0, [.664, .818]],
                ["Pelfgny Cnynpr", 6, 34, [.649, .831],
                    [.623, .821],
                    [.623, .852]
                ]
            ]
        }, {
            c: "Biretebhaq",
            b: "biretebhaq_qraznex_uvyy",
            a: "gsy-yh",
            f: [
                ["Pyncunz Whapgvba", 0, 2, [.365, .747],
                    [.421, .741],
                    [.416, .857]
                ],
                ["snxr2", 0, 0, [.453, .805]],
                ["snxr1", 0, 0, [.461, .708]],
                ["Jnaqfjbegu Ebnq", 6, 2, [.404, .747],
                    [.483,
                        .71
                    ],
                    [.465, .805]
                ],
                ["snxr0", 0, 0, [.423, .747]],
                ["Pyncunz Uvtu Fgerrg", 8, 2, [.443, .775],
                    [.491, .724],
                    [.515, .807]
                ],
                ["snxr0", 0, 0, [.452, .786]],
                ["snxr1", 0, 0, [.497, .727]],
                ["Qraznex Uvyy", 13, 2, [.524, .786],
                    [.603, .726],
                    [.593, .805]
                ],
                ["snxr0", 0, 0, [.565, .786]],
                ["snxr2", 0, 0, [.612, .805]],
                ["snxr1", 0, 0, [.632, .727]],
                ["Crpxunz Elr", 16, 2, [.592, .747],
                    [.646, .715],
                    [.627, .784]
                ],
                ["Dhrraf Ebnq Crpxunz", 18, 2, [.614, .716],
                    [.681, .686],
                    [.651, .749]
                ],
                ["snxr0", 0, 0, [.621, .705]],
                ["snxr0", 0, 0, [.635, .705]],
                ["snxr0", 0, 0, [.671, .654]],
                ["snxr2",
                    0, 0, [.697, .688]
                ],
                ["snxr1", 0, 0, [.714, .658]],
                ["snxr1", 0, 0, [.717, .65]],
                ["snxr1", 0, 0, [.717, .639]],
                ["Fheerl Dhnlf", 25, 2, [.671, .642],
                    [.706, .611],
                    [.697, .647]
                ]
            ]
        }, {
            c: "Biretebhaq",
            b: "biretebhaq_ebzsbeq",
            a: "gsy-nat",
            f: [
                ["Ebzsbeq", 0, 6, [.909, .167],
                    [.928, .196],
                    [.918, .194]
                ],
                ["snxr2", 0, 0, [.936, .196]],
                ["Rzrefba Cnex", 4, 6, [.929, .196],
                    [.951, .241],
                    [.947, .218]
                ],
                ["snxr0", 0, 0, [.942, .213]],
                ["snxr2", 0, 0, [.957, .239]],
                ["Hczvafgre", 9, 6, [.952, .213],
                    [.976, .289],
                    [.976, .239]
                ]
            ]
        }, {
            c: "Biretebhaq",
            b: "biretebhaq_puvatsbeq",
            a: "gsy-yfg",
            f: [
                ["Yvirecbby Fgerrg", 0, 1, [.608, .428],
                    [.615, .471],
                    [.642, .466]
                ],
                ["snxr0", 0, 1, [.635, .39]],
                ["snxr0", 0, 1, [.671, .39]],
                ["snxr0", 0, 1, [.684, .372]],
                ["snxr0", 0, 1, [.697, .35]],
                ["snxr2", 0, 1, [.661, .464]],
                ["snxr2", 0, 1, [.672, .444]],
                ["snxr1", 0, 1, [.615, .421]],
                ["snxr1", 0, 1, [.629, .411]],
                ["snxr1", 0, 1, [.648, .365]],
                ["Unpxarl Qbjaf", 7, 2, [.697, .313],
                    [.648, .311],
                    [.671, .313]
                ],
                ["snxr0", 0, 0, [.697, .297]],
                ["snxr2", 0, 0, [.675, .301]],
                ["snxr2", 0, 0, [.685, .287]],
                ["snxr2", 0, 0, [.689, .275]],
                ["snxr1", 0, 0, [.648, .302]],
                ["Pyncgba", 10, 23, [.712,
                        .276
                    ],
                    [.672, .285],
                    [.69, .263]
                ],
                ["Fg. Wnzrf Fgerrg", 13, 3, [.722, .262],
                    [.695, .266],
                    [.69, .233]
                ],
                ["snxr0", 0, 0, [.735, .242]],
                ["snxr1", 0, 0, [.71, .257]],
                ["Jnygunzfgbj Prageny", 15, 3, [.735, .223],
                    [.726, .184],
                    [.69, .191]
                ],
                ["Jbbq Fgerrg", 17, 4, [.735, .168],
                    [.732, .149],
                    [.69, .17]
                ],
                ["Uvtunzf Cnex", 20, 4, [.735, .1318],
                    [.738, .118],
                    [.689, .147]
                ],
                ["Puvatsbeq", 28, 5, [.735, .094],
                    [.744, .078],
                    [.69, .127]
                ]
            ]
        }, {
            c: "Biretebhaq",
            b: "biretebhaq_purfuhag",
            a: "gsy-yfg",
            f: [
                ["Yvirecbby Fgerrg", 0, 1, [.608, .428],
                    [.615, .471],
                    [.643, .467]
                ],
                ["snxr0",
                    0, 1, [.635, .39]
                ],
                ["snxr0", 0, 1, [.671, .39]],
                ["snxr1", 0, 1, [.615, .421]],
                ["snxr1", 0, 1, [.629, .411]],
                ["Orguany Terra BT", 3, 2, [.684, .372],
                    [.639, .383],
                    [.663, .462]
                ],
                ["snxr2", 0, 0, [.669, .449]],
                ["snxr2", 0, 0, [.671, .435]],
                ["Pnzoevqtr Urngu", 5, 2, [.694, .358],
                    [.646, .366],
                    [.67, .396]
                ],
                ["snxr0", 0, 0, [.697, .35]],
                ["Ybaqba Svryqf", 7, 2, [.697, .349],
                    [.648, .35],
                    [.671, .37]
                ],
                ["Unpxarl Qbjaf", 9, 2, [.697, .313],
                    [.648, .311],
                    [.671, .314]
                ],
                ["snxr0", 0, 0, [.697, .298]],
                ["snxr2", 0, 0, [.668, .303]],
                ["snxr1", 0, 0, [.648, .302]],
                ["Erpgbel Ebnq", 12,
                    2, [.688, .287],
                    [.63, .289],
                    [.652, .283]
                ],
                ["Fgbxr Arjvatgba", 13, 2, [.674, .267],
                    [.613, .272],
                    [.639, .264]
                ],
                ["snxr0", 0, 0, [.662, .249]],
                ["snxr1", 0, 0, [.595, .26]],
                ["Fgnzsbeq Uvyy", 15, 3, [.662, .233],
                    [.595, .222],
                    [.627, .247]
                ],
                ["Frira Fvfgref", 17, 3, [.662, .214],
                    [.594, .145],
                    [.588, .191]
                ],
                ["snxr2", 0, 0, [.58, .178]],
                ["Oehpr Tebir", 19, 3, [.662, .164],
                    [.593, .126],
                    [.578, .165]
                ],
                ["Juvgr Uneg Ynar", 21, 3, [.662, .149],
                    [.593, .109],
                    [.579, .147]
                ],
                ["Fvyire Fgerrg", 23, 4, [.662, .131],
                    [.593, .088],
                    [.58, .131]
                ],
                ["snxr1", 0, 0, [.593, .078]],
                ["Rqzbagba Terra",
                    25, 4, [.662, .114],
                    [.602, .059],
                    [.578, .115]
                ],
                ["snxr0", 0, 0, [.662, .098]],
                ["snxr2", 0, 0, [.582, .098]],
                ["Fbhguohel", 29, 5, [.672, .085],
                    [.615, .047],
                    [.586, .091]
                ],
                ["Ghexrl Fgerrg", 32, 6, [.685, .066],
                    [.633, .033],
                    [.597, .074]
                ],
                ["Gurbonyqf Tebir", 34, 7, [.698, .048],
                    [.657, .019],
                    [.609, .057]
                ],
                ["Purfuhag", 39, 8, [.709, .033],
                    [.685, .014],
                    [.621, .04]
                ]
            ]
        }, {
            c: "Biretebhaq",
            b: "biretebhaq_rasvryq_gbja",
            a: "gsy-yfg",
            f: [
                ["Rqzbagba Terra", 0, 4, [.662, .114],
                    [.594, .087],
                    [.578, .116]
                ],
                ["Ohfu Uvyy Cnex", 3, 5, [.662, .068],
                    [.593, .053],
                    [.579, .083]
                ],
                ["Rasvryq Gbja", 8, 5, [.662, .047],
                    [.591, .029],
                    [.579, .063]
                ]
            ]
        }, {
            c: "GSYEnvy",
            b: "gsyenvy_znvayvar",
            a: "gsy-yfg",
            f: [
                ["Yvirecbby Fgerrg", 1, 1, [.608, .428],
                    [.616, .475],
                    [.641, .458]
                ],
                ["snxr0", 0, 1, [.722, .428]],
                ["snxr2", 0, 1, [.71, .457]],
                ["snxr2", 0, 1, [.719, .453]],
                ["snxr2", 0, 1, [.76, .397]],
                ["snxr2", 0, 1, [.776, .395]],
                ["Fgengsbeq", 7, 23, [.796, .326],
                    [.776, .347],
                    [.809, .345]
                ],
                ["Znelynaq", 9, 3, [.814, .301],
                    [.808, .32],
                    [.82, .329]
                ],
                ["Sberfg Tngr", 11, 3, [.829, .279],
                    [.828, .304],
                    [.832, .316]
                ],
                ["Znabe Cnex", 13, 34, [.845, .257],
                    [.849, .29],
                    [.852, .287]
                ],
                ["Vysbeq", 16, 4, [.855, .243],
                    [.865, .274],
                    [.871, .261]
                ],
                ["Frira Xvatf", 18, 4, [.864, .231],
                    [.88, .257],
                    [.883, .244]
                ],
                ["Tbbqznlrf", 20, 4, [.874, .215],
                    [.896, .24],
                    [.894, .229]
                ],
                ["Punqjryy Urngu", 22, 5, [.893, .188],
                    [.912, .221],
                    [.906, .212]
                ],
                ["Ebzsbeq", 26, 6, [.909, .167],
                    [.928, .196],
                    [.918, .195]
                ],
                ["Tvqrn Cnex", 30, 6, [.924, .146],
                    [.944, .171],
                    [.931, .179]
                ],
                ["Unebyq Jbbq", 33, 6, [.934, .131],
                    [.957, .139],
                    [.943, .163]
                ],
                ["Oeragjbbq", 37, 9, [.957, .097],
                    [.957, .103],
                    [.954, .146]
                ],
                ["Furasvryq", 43, "S", [.976, .071],
                    [.957, .045],
                    [.966,
                        .129
                    ]
                ]
            ]
        }, {
            c: "QYE",
            b: "qye_jbbyjvpu",
            a: "gsy-yh",
            f: [
                ["Fgengsbeq Vagreangvbany", 0, 23, [.769, .29],
                    [.769, .326],
                    [.795, .309]
                ],
                ["snxr0", 0, 0, [.789, .29]],
                ["snxr0", 0, 0, [.806, .314]],
                ["Fgengsbeq", 2, 23, [.806, .326],
                    [.775, .341],
                    [.809, .346]
                ],
                ["Fgengsbeq Uvtu Fgerrg", 4, 23, [.806, .356],
                    [.783, .362],
                    [.822, .375]
                ],
                ["snxr2", 0, 0, [.828, .391]],
                ["Noorl Ebnq", 5, 23, [.806, .383],
                    [.794, .388],
                    [.83, .404]
                ],
                ["Jrfg Unz", 7, 23, [.806, .418],
                    [.802, .408],
                    [.83, .43]
                ],
                ["snxr1", 0, 0, [.809, .428]],
                ["Fgne Ynar", 8, 23, [.806, .459],
                    [.809, .478],
                    [.83, .485]
                ],
                ["snxr1", 0, 0, [.809, .491]],
                ["Pnaavat Gbja", 10, 23, [.806, .498],
                    [.814, .502],
                    [.83, .526]
                ],
                ["snxr0", 0, 0, [.812, .512]],
                ["snxr0", 0, 0, [.817, .512]],
                ["snxr2", 0, 0, [.836, .538]],
                ["snxr2", 0, 0, [.843, .54]],
                ["snxr1", 0, 0, [.822, .51]],
                ["snxr1", 0, 0, [.837, .548]],
                ["snxr1", 0, 0, [.842, .554]],
                ["Jrfg Fvyiregbja", 13, 3, [.851, .561],
                    [.846, .556],
                    [.852, .553]
                ],
                ["Cbagbba Qbpx", 15, 3, [.867, .583],
                    [.863, .555],
                    [.867, .577]
                ],
                ["Ybaqba Pvgl Nvecbeg", 17, 3, [.883, .606],
                    [.884, .557],
                    [.884, .598]
                ],
                ["snxr1", 0, 0, [.896, .556]],
                ["snxr1", 0, 0, [.9, .559]],
                ["Xvat Trbetr I",
                    19, 3, [.899, .629],
                    [.903, .564],
                    [.9, .619]
                ],
                ["snxr0", 0, 0, [.905, .636]],
                ["snxr2", 0, 0, [.906, .628]],
                ["snxr1", 0, 0, [.909, .578]],
                ["snxr1", 0, 0, [.911, .588]],
                ["Jbbyjvpu Nefrany", 23, 4, [.905, .681],
                    [.911, .614],
                    [.931, .628]
                ]
            ]
        }, {
            c: "QYE",
            b: "qye_yrjvfunz",
            a: "gsy-yh",
            f: [
                ["Yrjvfunz", 0, 23, [.745, .761],
                    [.752, .727],
                    [.767, .794]
                ],
                ["Ryirefba Ebnq", 2, 23, [.745, .745],
                    [.748, .715],
                    [.768, .77]
                ],
                ["snxr1", 0, 0, [.746, .709]],
                ["snxr1", 0, 0, [.746, .702]],
                ["Qrcgsbeq Oevqtr", 3, 23, [.745, .727],
                    [.748, .696],
                    [.768, .749]
                ],
                ["Terrajvpu", 5, 23, [.745, .711],
                    [.753, .683],
                    [.767, .726]
                ],
                ["Phggl Fnex", 7, 23, [.745, .692],
                    [.758, .672],
                    [.767, .705]
                ],
                ["Vfynaq Tneqraf", 9, 2, [.745, .662],
                    [.769, .643],
                    [.767, .682]
                ],
                ["snxr1", 0, 0, [.77, .637]],
                ["snxr1", 0, 0, [.769, .631]],
                ["Zhqpuhgr", 10, 2, [.745, .646],
                    [.765, .621],
                    [.767, .661]
                ],
                ["Pebffuneobhe", 11, 2, [.745, .629],
                    [.757, .6],
                    [.767, .638]
                ],
                ["Fbhgu Dhnl", 13, 2, [.745, .613],
                    [.748, .579],
                    [.769, .616]
                ],
                ["Ureba Dhnlf", 15, 2, [.745, .596],
                    [.743, .567],
                    [.768, .594]
                ],
                ["snxr1", 0, 0, [.741, .562]],
                ["Pnanel Junes", 16, 2, [.745, .571],
                    [.74, .545],
                    [.769, .574]
                ],
                ["Jrfg Vaqvn Dhnl",
                    17, 2, [.745, .546],
                    [.741, .533],
                    [.767, .557]
                ],
                ["snxr2", 0, 0, [.769, .548]],
                ["snxr1", 0, 0, [.741, .517]],
                ["snxr1", 0, 0, [.743, .512]],
                ["snxr1", 0, 0, [.746, .507]],
                ["snxr1", 0, 0, [.749, .502]],
                ["Cbcyne", 19, 2, [.745, .512],
                    [.753, .503],
                    [.774, .54]
                ],
                ["snxr2", 0, 0, [.777, .533]],
                ["snxr1", 0, 0, [.758, .503]],
                ["snxr1", 0, 0, [.765, .499]],
                ["snxr1", 0, 0, [.768, .491]],
                ["snxr1", 0, 0, [.769, .482]],
                ["Nyy Fnvagf", 21, 2, [.745, .487],
                    [.767, .477],
                    [.776, .508]
                ],
                ["Ynatqba Cnex", 22, 2, [.745, .47],
                    [.762, .463],
                    [.775, .487]
                ],
                ["Qribaf Ebnq", 23, 2, [.745, .453],
                    [.758,
                        .452
                    ],
                    [.776, .469]
                ],
                ["snxr1", 0, 0, [.749, .429]],
                ["Obj Puhepu", 25, 2, [.745, .425],
                    [.749, .424],
                    [.776, .416]
                ],
                ["snxr0", 0, 0, [.745, .411]],
                ["snxr2", 0, 0, [.779, .401]],
                ["snxr1", 0, 0, [.75, .417]],
                ["Chqqvat Zvyy Ynar", 27, 23, [.776, .367],
                    [.763, .384],
                    [.793, .379]
                ],
                ["snxr1", 0, 0, [.772, .36]],
                ["Fgengsbeq", 29, 23, [.806, .326],
                    [.772, .347],
                    [.811, .353]
                ]
            ]
        }, {
            c: "QYE",
            b: "qye_orpxgba",
            a: "gsy-yh",
            f: [
                ["Gbjre Tngrjnl", 0, 1, [.639, .528],
                    [.635, .512],
                    [.658, .539]
                ],
                ["snxr0", 0, 1, [.65, .512]],
                ["snxr1", 0, 0, [.647, .503]],
                ["Funqjryy", 2, 2, [.677, .512],
                    [.663, .502],
                    [.697, .541]
                ],
                ["Yvzrubhfr", 4, 2, [.702, .512],
                    [.696, .501],
                    [.715, .54]
                ],
                ["Jrfgsreel", 6, 2, [.722, .512],
                    [.725, .503],
                    [.737, .54]
                ],
                ["Cbcyne", 8, 2, [.745, .512],
                    [.753, .502],
                    [.774, .541]
                ],
                ["Oynpxjnyy", 10, 2, [.76, .512],
                    [.777, .502],
                    [.791, .54]
                ],
                ["Rnfg Vaqvn", 11, 23, [.781, .512],
                    [.789, .502],
                    [.808, .54]
                ],
                ["Pnaavat Gbja", 13, 23, [.796, .512],
                    [.814, .502],
                    [.826, .54]
                ],
                ["snxr1", 0, 0, [.823, .509]],
                ["snxr1", 0, 0, [.831, .514]],
                ["Eblny Ivpgbevn", 15, 3, [.848, .512],
                    [.839, .514],
                    [.869, .539]
                ],
                ["snxr0", 0, 0, [.871, .512]],
                ["Phfgbz Ubhfr",
                    17, 3, [.882, .528],
                    [.85, .515],
                    [.888, .54]
                ],
                ["Cevapr Ertrag", 18, 3, [.895, .547],
                    [.863, .514],
                    [.906, .541]
                ],
                ["Eblny Nyoreg", 20, 3, [.908, .565],
                    [.879, .515],
                    [.923, .541]
                ],
                ["Orpxgba Cnex", 21, 3, [.921, .583],
                    [.896, .515],
                    [.942, .539]
                ],
                ["Plcehf", 23, 3, [.934, .602],
                    [.911, .515],
                    [.96, .54]
                ],
                ["snxr2", 0, 0, [.966, .537]],
                ["snxr2", 0, 0, [.97, .529]],
                ["snxr1", 0, 0, [.916, .512]],
                ["snxr1", 0, 0, [.919, .508]],
                ["Tnyyvbaf Ernpu", 24, 3, [.947, .62],
                    [.92, .503],
                    [.971, .509]
                ],
                ["snxr1", 0, 0, [.918, .497]],
                ["snxr1", 0, 0, [.916, .493]],
                ["snxr1", 0, 0, [.912, .491]],
                ["Orpxgba", 27, 3, [.96, .638],
                    [.9, .491],
                    [.97, .486]
                ]
            ]
        }, {
            c: "QYE",
            b: "qye_onax",
            a: "gsy-yh",
            f: [
                ["Onax", 0, 1, [.589, .496],
                    [.594, .502],
                    [.61, .53]
                ],
                ["snxr0", 0, 1, [.637, .496]],
                ["snxr0", 0, 1, [.6503, .512]],
                ["snxr2", 0, 1, [.623, .518]],
                ["snxr2", 0, 1, [.654, .519]],
                ["snxr2", 0, 1, [.667, .535]],
                ["snxr2", 0, 1, [.676, .539]],
                ["Funqjryy", 3, 2, [.677, .512],
                    [.663, .502],
                    [.697, .539]
                ]
            ]
        }, {
            c: "QYE",
            b: "qye_jrfgsreel",
            a: "gsy-yh",
            f: [
                ["Jrfg Vaqvn Dhnl", 0, 2, [.745, .546],
                    [.741, .532],
                    [.768, .557]
                ],
                ["snxr0", 0, 0, [.745, .524]],
                ["snxr0", 0, 0, [.736, .512]],
                ["snxr2",
                    0, 0, [.768, .549]
                ],
                ["snxr2", 0, 0, [.761, .541]],
                ["snxr1", 0, 0, [.74, .517]],
                ["snxr1", 0, 0, [.738, .511]],
                ["snxr1", 0, 0, [.735, .507]],
                ["snxr1", 0, 0, [.732, .502]],
                ["Jrfgsreel", 2, 2, [.722, .512],
                    [.724, .502],
                    [.736, .541]
                ]
            ]
        }, {
            c: "Genz",
            b: "genz_jvzoyrqba",
            a: "genz",
            f: [
                ["Jvzoyrqba", 0, "T", [.323, .772],
                    [.349, .822],
                    [.395, .936]
                ],
                ["Qhaqbanyq Ebnq", 1, "T", [.323, .812],
                    [.358, .828],
                    [.406, .934]
                ],
                ["snxr0", 0, 0, [.323, .832]],
                ["Zregba Cnex", 3, "T", [.347, .866],
                    [.368, .838],
                    [.417, .935]
                ],
                ["snxr0", 0, 0, [.382, .918]],
                ["Zbeqra Ebnq", 4, "T", [.392, .919],
                    [.382, .849],
                    [.432, .955]
                ],
                ["snxr2", 0, 0, [.435, .959]],
                ["Cuvccf Oevqtr", 6, "T", [.414, .919],
                    [.394, .859],
                    [.445, .96]
                ],
                ["Orytenir Jnyx", 7, "T", [.438, .919],
                    [.407, .87],
                    [.456, .96]
                ],
                ["Zvgpunz", 9, "T", [.463, .919],
                    [.425, .884],
                    [.468, .961]
                ],
                ["Zvgpunz Whapgvba", 12, "T", [.489, .919],
                    [.45, .905],
                    [.48, .961]
                ],
                ["Orqqvatgba Ynar", 14, "T", [.52, .919],
                    [.463, .916],
                    [.492, .96]
                ],
                ["snxr1", 0, 0, [.474, .924]],
                ["Gurencvn Ynar", 16, "T", [.547, .919],
                    [.485, .925],
                    [.504, .96]
                ],
                ["Nzcrer Jnl", 17, "T", [.57, .919],
                    [.498, .926],
                    [.516, .961]
                ],
                ["Jnqqba Znefu",
                    18, "T", [.593, .919],
                    [.512, .925],
                    [.528, .961]
                ],
                ["Jnaqyr Cnex", 20, "T", [.616, .919],
                    [.526, .926],
                    [.54, .96]
                ],
                ["snxr0", 0, 0, [.625, .919]],
                ["snxr1", 0, 0, [.534, .925]],
                ["Errirf Pbeare", 22, "T", [.633, .907],
                    [.54, .917],
                    [.552, .961]
                ],
                ["snxr0", 0, 0, [.646, .887]],
                ["snxr1", 0, 0, [.552, .903]],
                ["snxr2", 0, 0, [.56, .96]],
                ["Pragenyr", 24, "T", [.657, .887],
                    [.559, .902],
                    [.571, .946]
                ],
                ["Jrfg Peblqba", 26, "T", [.679, .887],
                    [.573, .902],
                    [.58, .942]
                ],
                ["Jryyrfyrl Ebnq", 28, "T", [.697, .887],
                    [.587, .902],
                    [.589, .948]
                ],
                ["snxr0", 0, 0, [.706, .889]],
                ["snxr0",
                    0, 0, [.711, .896]
                ],
                ["snxr0", 0, 0, [.714, .911]],
                ["snxr0", 0, 0, [.719, .918]],
                ["snxr1", 0, 0, [.591, .905]],
                ["snxr1", 0, 0, [.594, .914]],
                ["snxr1", 0, 0, [.596, .92]],
                ["snxr1", 0, 0, [.601, .925]],
                ["snxr2", 0, 0, [.597, .96]],
                ["Rnfg Peblqba", 30, "T", [.726, .919],
                    [.606, .926],
                    [.604, .959]
                ],
                ["Yronaba Ebnq", 32, "T", [.741, .919],
                    [.619, .925],
                    [.615, .96]
                ],
                ["Fnaqvynaqf", 33, "T", [.756, .919],
                    [.631, .926],
                    [.627, .96]
                ],
                ["snxr0", 0, 0, [.769, .919]],
                ["Yyblq Cnex", 36, "T", [.781, .935],
                    [.651, .926],
                    [.641, .961]
                ],
                ["Pbbzor Ynar", 39, "T", [.789, .949],
                    [.672, .927],
                    [.653, .961]
                ],
                ["snxr0", 0, 0, [.797, .959]],
                ["Teniry Uvyy", 41, "T", [.809, .959],
                    [.688, .927],
                    [.665, .961]
                ],
                ["Nqqvatgba Ivyyntr", 43, "T", [.84, .959],
                    [.708, .927],
                    [.676, .961]
                ],
                ["snxr1", 0, 0, [.717, .926]],
                ["Svryqjnl", 45, "T", [.87, .959],
                    [.729, .936],
                    [.689, .959]
                ],
                ["Xvat Urael'f Qevir", 46, "T", [.9, .959],
                    [.743, .948],
                    [.701, .96]
                ],
                ["Arj Nqqvatgba", 49, "T", [.932, .959],
                    [.76, .961],
                    [.713, .96]
                ]
            ]
        }, {
            c: "Genz",
            b: "genz_orpxraunz",
            a: "genz",
            f: [
                ["Orpxraunz Whapgvba", 0, "T", [.892, .791],
                    [.741, .838],
                    [.703, .861]
                ],
                ["Orpxraunz Ebnq", 2, "T", [.863,
                        .791
                    ],
                    [.729, .848],
                    [.696, .871]
                ],
                ["Nirahr Ebnq", 4, "T", [.834, .791],
                    [.719, .858],
                    [.687, .884]
                ],
                ["Ovexorpx", 6, "T", [.805, .791],
                    [.707, .868],
                    [.679, .894]
                ],
                ["snxr0", 0, 0, [.786, .791]],
                ["snxr0", 0, 0, [.774, .806]],
                ["Uneevatgba Ebnq", 9, "T", [.774, .815],
                    [.693, .879],
                    [.671, .907]
                ],
                ["Neran", 11, "T", [.774, .853],
                    [.68, .889],
                    [.662, .918]
                ],
                ["Jbbqfvqr", 12, "T", [.774, .867],
                    [.672, .897],
                    [.653, .932]
                ],
                ["Oynpxubefr Ynar", 13, "T", [.774, .885],
                    [.663, .905],
                    [.646, .942]
                ],
                ["Nqqvfpbzor", 15, "T", [.774, .902],
                    [.653, .912],
                    [.637, .952]
                ],
                ["snxr0", 0, 0, [.774,
                    .912
                ]],
                ["snxr0", 0, 0, [.768, .919]],
                ["snxr1", 0, 0, [.637, .924]],
                ["snxr2", 0, 0, [.633, .957]],
                ["Fnaqvynaqf", 17, "T", [.756, .919],
                    [.63, .926],
                    [.627, .959]
                ],
                ["Yronaba Ebnq", 19, "T", [.741, .919],
                    [.618, .926],
                    [.616, .962]
                ],
                ["Rnfg Peblqba", 21, "T", [.726, .919],
                    [.606, .925],
                    [.603, .96]
                ],
                ["Trbetr Fgerrg", 23, "T", [.694, .919],
                    [.581, .926],
                    [.587, .961]
                ],
                ["Puhepu Fgerrg", 25, "T", [.663, .919],
                    [.558, .927],
                    [.576, .96]
                ]
            ]
        }, {
            c: "Genz",
            b: "genz_puhepu_jnaqyr",
            a: "genz",
            f: [
                ["Puhepu Fgerrg", 0, "T", [.663, .919],
                    [.557, .926],
                    [.575, .96]
                ],
                ["Jnaqyr Cnex",
                    3, "T", [.616, .919],
                    [.525, .926],
                    [.552, .961]
                ]
            ]
        }, {
            c: "Genz",
            b: "genz_ryzref",
            a: "genz",
            f: [
                ["Neran", 0, "T", [.774, .853],
                    [.681, .89],
                    [.661, .919]
                ],
                ["snxr0", 0, 0, [.774, .84]],
                ["snxr0", 0, 0, [.779, .834]],
                ["snxr1", 0, 0, [.686, .885]],
                ["snxr2", 0, 0, [.665, .915]],
                ["Ryzref Raq", 3, "T", [.827, .834],
                    [.708, .884],
                    [.676, .915]
                ]
            ]
        }, {
            c: "Genz",
            b: "genz_puhepu_pragenyr",
            a: "genz",
            f: [
                ["Puhepu Fgerrg", 0, "T", [.663, .919],
                    [.558, .926],
                    [.576, .96]
                ],
                ["snxr0", 0, 0, [.646, .919]],
                ["snxr0", 0, 0, [.641, .911]],
                ["snxr0", 0, 0, [.64, .898]],
                ["snxr0", 0, 0, [.646, .887]],
                ["snxr1", 0, 0, [.551, .924]],
                ["snxr1", 0, 0, [.546, .917]],
                ["snxr1", 0, 0, [.546, .91]],
                ["snxr1", 0, 0, [.552, .903]],
                ["snxr2", 0, 0, [.571, .96]],
                ["snxr2", 0, 0, [.57, .956]],
                ["snxr2", 0, 0, [.568, .951]],
                ["Pragenyr", 2, "T", [.657, .887],
                    [.559, .903],
                    [.57, .947]
                ],
                ["Jrfg Peblqba", 4, "T", [.679, .887],
                    [.573, .901],
                    [.58, .941]
                ]
            ]
        }, {
            c: "Rzvengrf",
            b: "rzvengrf_nve_yvar",
            a: "rzvengrf",
            f: [
                ["Rzvengrf Terrajvpu Cravafhyn", 0, "E", [.796, .572],
                    [.809, .563],
                    [.816, .595]
                ],
                ["snxr0", 0, 0, [.805, .572]],
                ["snxr0", 0, 0, [.838, .525]],
                ["snxr2", 0, 0, [.828, .595]],
                ["snxr2",
                    0, 0, [.858, .547]
                ],
                ["Rzvengrf Eblny Qbpxf", 7, "E", [.848, .525],
                    [.838, .52],
                    [.869, .548]
                ]
            ]
        }
    ];

function oa(a) {
    this.G = a;
    this.data = []
}

function pa(a, b) {
    a.data.push(b);
    for (var d = a.data.length - 1, e, c; 0 < d;)
        if (e = d - 1 >>> 1, 0 > a.G(a.data[d], a.data[e])) c = a.data[e], a.data[e] = a.data[d], a.data[d] = c, d = e;
        else break
}

function qa(a) {
    if (0 === a.data.length) throw "Empty queue";
    var b, d;
    d = a.data[0];
    b = a.data.pop();
    if (0 < a.data.length) {
        a.data[0] = b;
        b = 0;
        var e, c, f, g;
        for (e = a.data.length - 1;;)
            if (c = (b << 1) + 1, g = c + 1, f = b, c <= e && 0 > a.G(a.data[c], a.data[f]) && (f = c), g <= e && 0 > a.G(a.data[g], a.data[f]) && (f = g), f !== b) c = a.data[f], a.data[f] = a.data[b], a.data[b] = c, b = f;
            else break
    }
    return d
}

function ra() {
    var a = 1 / 0;
    this.F = {};
    this.I = function(a, d) {
        this.F[a] = d
    };
    this.K = function(b, d, e) {
        e = void 0 === e ? !1 : e;
        var c = new oa(function(a, b) {
                return a[0] - b[0]
            }),
            f = {},
            g = {},
            h = [],
            l, m;
        for (l in this.F) l === b ? (f[l] = 0, pa(c, [0, l])) : f[l] = a, g[l] = null;
        for (; c.data.length;) {
            b = qa(c)[1];
            if (!e && b === d) {
                for (; g[b];) h.push([b, f[b]]), b = g[b];
                return h
            }
            if (b && f[b] !== a)
                for (m in this.F[b]) l = f[b] + this.F[b][m], l < f[m] && (f[m] = l, g[m] = b, pa(c, [l, m]))
        }
        return e ? f : []
    }
};
var A = !1,
    B = !1,
    C = !0,
    D = !0,
    sa = [
        [function() {
            A = !A
        }, A, "times"],
        [function() {
            C = !C
        }, C, "heat map"],
        [function() {
            B = !B
        }, B, "dual times"],
        [function() {
            D = !D
        }, D, "dual heat map"]
    ],
    E = !1,
    F = !0,
    H = new Set,
    I = 5,
    J = !1,
    ta = [
        [0, "standard"],
        [1, "alternate 1"],
        [2, "alternate 2"]
    ],
    K = 0,
    ua = [0, 15].concat([I]);
ua.sort(function(a, b) {
    return a - b
});

function L(a, b, d, e, c, f, g) {
    this.name = b;
    this.i = d;
    this.u = e;
    this.H = c;
    this.J = f;
    this.coords = g;
    this.prev = [!1, 1E3];
    this.next = [!1, 1E3];
    this.s = this.A = !1;
    this.l = new Map;
    this.g = !1;
    this.id = a.length + 2097152;
    a.push(this);
    return this
}
L.prototype.connect = function(a, b) {
    this.l.set(a, b);
    a.l.set(this, b)
};

function va(a, b, d, e) {
    a.next = [b, d];
    if (void 0 === e || e) b.prev = [a, d]
}
L.prototype.D = function(a) {
    this.s = a;
    a.A = this
};

function M(a, b) {
    this.name = b[0].name;
    this.H = b[0].H;
    this.J = !1;
    for (var d = r([0, 0]), e = d.next().value, d = d.next().value, c = 0; c < b.length; c++) e += b[c].coords[0], d += b[c].coords[1];
    e /= b.length;
    d /= b.length;
    this.coords = [e, d];
    this.l = new Map(b.map(function(a) {
        return [a, 0]
    }));
    for (e = 0; e < b.length; e++) b[e].g = this;
    this.id = a.length + 4194304;
    a.push(this);
    return this
}

function wa(a, b, d, e) {
    this.i = b;
    this.u = d;
    this.coords = e;
    this.s = this.A = !1;
    this.id = a.length + 8388608;
    a.push(this);
    return this
}
wa.prototype.D = L.prototype.D;

function N(a, b) {
    return ++b ? String.fromCharCode(("[" > a ? 91 : 123) > (a = a.charCodeAt() + 13) ? a : a - 26) : a.replace(/[a-zA-Z]/g, N)
}

function xa() {
    for (var a = new Map(na.map(function(a) {
            return [N(a[0]), N(a[1])]
        })), b = new Set(ka.map(function(a) {
            return N(a)
        })), d = new Set(la.map(function(a) {
            return N(a)
        })), e = {}, c = r(ma), f = c.next(); !f.done; f = c.next()) {
        var g = r(f.value),
            f = g.next().value,
            g = g.next().value;
        key = N(f);
        key in e || (e[key] = new Set);
        e[key].add(N(g))
    }
    console.log(e);
    c = [];
    f = [];
    for (g = 0; g < z.length; g++) {
        var h = N(z[g].c);
        if (!H.has(h)) {
            var l = N(z[g].b),
                m = !1;
            if (J) {
                if (!b.has(h)) continue;
                if (d.has(l)) continue;
                l in e && (m = e[l])
            }
            for (var p = N(z[g].a), t =
                    z[g].f, w = !1, G = !1, aa = 1 / 0, ya = 0; ya < t.length; ya++) {
                var u = t[ya],
                    P = N(u[0]);
                if (J && m && m.has(P)) G = w = !1;
                else {
                    var Ma = u[2];
                    if (F || 1 !== Ma)
                        if ("f" === P[0] && "a" === P[1]) {
                            if (4 !== P.length && P[4] == K) {
                                var ba = new wa(f, h, l, u[3]);
                                w && w.D(ba);
                                w = ba
                            }
                        } else u.length < 4 + K || (ba = u[1], u = new L(c, P, h, l, Ma, p, u[3 + K]), G && (aa = ba - aa, a.get(u.name) === G.name ? va(G, u, aa, !1) : va(G, u, aa), w.D(u)), w = G = u, aa = ba);
                    else G = w = !1
                }
            }
        }
    }
    return [c, f]
}

function za(a) {
    for (var b = new Map(ia.map(function(a) {
            return [N(a[0]), N(a[1])]
        })), d = new Map, e = new Map(ja.map(function(a) {
            return [N(a[0]), N(a[1])]
        })), c = new Map, f = 0; f < a.length; f++) {
        var g = a[f],
            h = g.name;
        c.has(h) ? c.get(h).push(g) : c.set(h, [g])
    }
    a = r(c);
    for (f = a.next(); !f.done; f = a.next())
        if (g = r(f.value), f = g.next().value, g = g.next().value, b.has(f) && c.get(b.get(f)) && (d.set(b.get(f), f), c.set(f, c.get(f).concat(c.get(b.get(f)))), c["delete"](b.get(f))), e.has(f) && (f = c.get(e.get(f))))
            for (g = r(g), h = g.next(); !h.done; h = g.next())
                for (var h =
                        h.value, l = r(f), m = l.next(); !m.done; m = l.next()) h.connect(m.value, I);
    b = [];
    c = r(c);
    for (e = c.next(); !e.done; e = c.next()) a = r(e.value), e = a.next().value, a = a.next().value, !d.has(e) && 1 < a.length && new M(b, a);
    return b
}

function Aa(a) {
    function b(a, b, c) {
        a.has(b) ? a.get(b).add(c) : a.set(b, new Set([c]))
    }
    var d = new Map,
        e = new Map;
    a = r(a);
    for (var c = a.next(); !c.done; c = a.next()) {
        var c = c.value,
            f = Ba(c.name.toLowerCase());
        c.g && (c = c.g);
        e.set(f, c);
        for (var f = r(f.split(" ")), g = f.next(); !g.done; g = f.next()) {
            g = g.value;
            b(d, g, c);
            for (var h = 2; h < g.length; h++) b(d, g.substring(0, h), c)
        }
    }
    return [d, e]
}

function Ca(a, b) {
    return a.filter(function(a) {
        return !a.g
    }).concat(b).sort(function(a, b) {
        return a.coords[0] - b.coords[0]
    })
};
var Da = new Map([
    ["Bakerloo", "#ae520e"],
    ["Central", "#ee2622"],
    ["Circle", "#fdd005"],
    ["District", "#088137"],
    ["HammersmithCity", "#f468a1"],
    ["Jubilee", "#94989a"],
    ["Metropolitan", "#960154"],
    ["Northern", "#231f20"],
    ["Piccadilly", "#263d96"],
    ["Victoria", "#2d9edf"],
    ["WaterlooCity", "#87d6b5"],
    ["Overground", "#f47422"],
    ["TFLRail", "#293f97"],
    ["DLR", "#21b5b0"],
    ["Tram", "#72c933"],
    ["Emirates", "#ee2622"]
]);
var Ea = new Set(["S", "W", "E", "T"]),
    Fa = new Map([
        ["E", [3.5, 3.5]],
        ["T", [1.5, 1.5]]
    ]),
    Ga = {
        "tfl-eus": {
            C: {
                "1-3": 3.1,
                "1-4": 3.5,
                "1-5": 4.4,
                "1-6": 4.8,
                "1-7": 5.1,
                "1-8": 6.2
            },
            B: {
                "1-2": 2.4,
                "1-3": 2.8,
                "1-4": 2.8,
                "1-5": 3.1,
                "1-6": 3.1,
                "1-7": 4,
                "1-8": 4
            }
        },
        "tfl-lu": {
            C: {
                1: 2.4,
                "1-2": 2.9,
                "1-3": 3.3,
                "1-4": 3.9,
                "1-5": 4.7,
                "1-6": 5.1,
                "1-7": 5.6,
                "1-8": 6.9,
                "1-9": 7,
                2: 1.7,
                3: 1.7,
                4: 1.7,
                5: 1.7,
                6: 1.7,
                "2-3": 1.7,
                "3-4": 1.7,
                "4-5": 1.7,
                "5-6": 1.7,
                "2-4": 2.4,
                "3-5": 2.4,
                "4-6": 2.4,
                "2-5": 2.8,
                "3-6": 2.8,
                "2-6": 2.8,
                "2-7": 4,
                "2-8": 4.7,
                "2-9": 4.7,
                "3-7": 3.4,
                "3-8": 4,
                "3-9": 4.1,
                "4-7": 2.8,
                "4-8": 3.4,
                "4-9": 3.5,
                "5-7": 2.4,
                "5-8": 2.8,
                "5-9": 2.9,
                "6-7": 1.7,
                "6-8": 2.4,
                "6-9": 2.4,
                7: 1.7,
                8: 1.7,
                9: 1.7,
                "7-8": 1.7,
                "8-9": 1.7,
                "7-9": 1.9
            },
            B: {
                1: 2.4,
                "1-2": 2.4,
                "1-3": 2.8,
                "1-4": 2.8,
                "1-5": 3.1,
                "1-6": 3.1,
                "1-7": 4,
                "1-8": 4,
                "1-9": 4.1,
                2: 1.5,
                3: 1.5,
                4: 1.5,
                5: 1.5,
                6: 1.5,
                "2-3": 1.5,
                "3-4": 1.5,
                "4-5": 1.5,
                "5-6": 1.5,
                "2-4": 1.5,
                "3-5": 1.5,
                "4-6": 1.5,
                "2-5": 1.5,
                "3-6": 1.5,
                "2-6": 1.5,
                "2-7": 2.8,
                "2-8": 2.9,
                "2-9": 2.9,
                "3-7": 1.8,
                "3-8": 1.8,
                "3-9": 1.8,
                "4-7": 1.8,
                "4-8": 1.8,
                "4-9": 1.8,
                "5-7": 1.8,
                "5-8": 1.8,
                "5-9": 1.8,
                "6-7": 1.5,
                "6-8": 1.7,
                "6-9": 1.8,
                7: 1.5,
                8: 1.5,
                9: 1.5,
                "7-8": 1.5,
                "8-9": 1.5,
                "7-9": 1.6
            }
        },
        "tfl-lst": {
            C: {
                "1-2": 2.7,
                "1-3": 3.3,
                "1-4": 3.9,
                "1-5": 4.7,
                "1-6": 5.1,
                "1-7": 5.6,
                "1-8": 6.1,
                "1-9": 7.6
            },
            B: {
                "1-2": 2.2,
                "1-3": 2.5,
                "1-4": 2.8,
                "1-5": 3.1,
                "1-6": 3.1,
                "1-7": 4,
                "1-8": 4,
                "1-9": 5.3
            }
        },
        "tfl-ang": {
            C: {
                "1-2": 2.9,
                "1-3": 3.3,
                "1-4": 3.9,
                "1-5": 4.7,
                "1-6": 5.1,
                "1-7": 5.6,
                "1-8": 6.9,
                "1-9": 8.2,
                2: 1.7,
                3: 1.7,
                4: 1.7,
                5: 1.7,
                6: 1.7,
                "2-3": 1.7,
                "3-4": 1.7,
                "4-5": 1.7,
                "5-6": 1.7,
                "2-4": 2.4,
                "3-5": 2.4,
                "4-6": 2.4,
                "2-5": 2.8,
                "3-6": 2.8,
                "2-6": 2.8,
                "2-7": 4.3,
                "2-8": 5.5,
                "2-9": 6.6,
                "3-7": 3.4,
                "3-8": 4.3,
                "3-9": 5.5,
                "4-7": 2.8,
                "4-8": 3.4,
                "4-9": 4,
                "5-7": 2.3,
                "5-8": 2.8,
                "5-9": 3.4,
                "6-7": 1.7,
                "6-8": 2.4,
                "6-9": 2.8,
                7: 1.7,
                8: 1.7,
                9: 1.7,
                "7-8": 1.7,
                "8-9": 1.7
            },
            B: {
                "1-2": 2.4,
                "1-3": 2.8,
                "1-4": 2.8,
                "1-5": 3.1,
                "1-6": 3.1,
                "1-7": 4,
                "1-8": 4,
                "1-9": 5.3,
                2: 1.5,
                3: 1.5,
                4: 1.5,
                5: 1.5,
                6: 1.5,
                "2-3": 1.5,
                "3-4": 1.5,
                "4-5": 1.5,
                "5-6": 1.5,
                "2-4": 1.9,
                "3-5": 1.9,
                "4-6": 1.9,
                "2-5": 2.4,
                "3-6": 2.4,
                "2-6": 2.6,
                "2-7": 2.7,
                "2-8": 3.3,
                "2-9": 4.4,
                "3-7": 2.7,
                "3-8": 2.7,
                "3-9": 3.3,
                "4-7": 2.4,
                "4-8": 2.4,
                "4-9": 2.7,
                "5-7": 1.9,
                "5-8": 2,
                "5-9": 2.7,
                "6-7": 1.5,
                "6-8": 1.7,
                "6-9": 2.4,
                7: 1.4,
                8: 1.4,
                9: 1.4,
                "7-8": 1.4,
                "8-9": 1.4
            }
        }
    };
var Ha = !1,
    Ia = ["map", "search", "help", "options"];

function Ja(a, b) {
    b = void 0 === b ? !1 : b;
    a === Ha ? "map" === a ? (Ka(), resetZoom()) : Ja("map") : ($(La(Ha, !0)).removeClass(".B0".slice(1)), $(La(a, !0)).addClass(".B0".slice(1)), b ? $("#A9_" + Ha).hide() : $("#A9_" + Ha).fadeOut("fast"), "map" !== a && (b ? $("#A9_" + a).show() : $("#A9_" + a).fadeIn("fast")), "search" === a && $("#A1f").focus(), Ha = a, "map" === a ? $(La("map", !0) + " .B10").text("reset") : $(La("map", !0) + " .B10").text("map"))
}

function Na(a) {
    a.stopPropagation();
    mode_button = $(a.currentTarget);
    mode = mode_button.data("mode");
    Ja(mode)
}

function La(a, b) {
    return (void 0 === b ? 0 : b) ? "#A15_" + a : "#A15".slice(1) + "_" + a
};

function O(a) {
    a = $(a)[0];
    var b = r([a.width, a.height]),
        d = b.next().value,
        b = b.next().value;
    a.getContext("2d").clearRect(0, 0, d, b)
}

function Ba(a) {
    return a.replace("+", "and").replace("&", "and").replace("'", "").replace(".", "").replace("(", "").replace(")", "")
}

function Oa() {
    $("#A1c").html("tuber");
    $("#A1d").html("map");
    $("#A19").empty();
    $("#A1e").html("&nbsp;");
    $("#A1a").html("&nbsp;");
    O("#A18")
}

function Q() {
    R[0] && Pa(R[0]);
    R = [!1, !1, !1];
    for (var a = r(["#A23", "#A24"]), b = a.next(); !b.done; b = a.next()) b = $(b.value), b.removeClass(), b.html("&nbsp;");
    $("#A23").html("london tube map");
    $("#A24").html("click a station");
    Oa();
    O("#A25");
    O("#A8");
    $("#A10").removeClass(".B3".slice(1));
    Qa()
}

function Ka() {
    Ra[0] && (Sa(), $(".B1").addClass(".B4".slice(1)));
    Q()
}

function Ta() {
    Q();
    O("#A10");
    S = [];
    Ua = [];
    T = [];
    Va = !1;
    Wa = new Map;
    Xa = new Map;
    U = [];
    Ya = new Map;
    Za = new Map
}

function $a() {
    return !0;//this == top ? ($("#A3").empty(), window.location.replace("https://tubermap.com"), !1) : !0
}

function ab(a) {
    var b = new Set(a.map(function(a) {
        return a[0].J
    }));
    b["delete"](!1);
    b["delete"]("emirates");
    b["delete"]("tram");
    var d = b.size,
        e = a.map(function(a) {
            return a[0].H
        });
    a = 0;
    for (var c = 10, f = new Map, g = 0; g < e.length; g++) {
        var h = e[g];
        Ea.has(h) && (f.has(h) ? f.set(h, f.get(h) + 1) : f.set(h, 1))
    }
    if (0 < d)
        for (g = 0; g < e.length; g++)
            if (h = e[g], !Ea.has(h)) {
                if (10 < h) {
                    var l = Math.floor(h / 10),
                        m = h % 10;
                    if (0 === g) {
                        if (e[g + 1] == h) {
                            if (2 == e.length) {
                                a = c = 3;
                                break
                            }
                            e = e.slice(1);
                            g = 0;
                            continue
                        }
                        e[g] = e[g + 1] <= l ? l : m
                    } else e[g] = e[g - 1] <= l ? l : m;
                    h = e[g]
                }
                c =
                    Math.min(h, c);
                a = Math.max(h, a)
            }
    e = "zone";
    g = [0, 0];
    if (0 < d) {
        fz = c === a ? "" + c : c + "-" + a;
        h = !1;
        if (1 === d)
            for (b = r(b).next(); !b.done;) {
                h = b.value;
                "tfl-eus" === h && 1 < c && (h = "tfl-lu");
                "tfl-lst" === h && 1 < c && (h = "tfl-ang");
                break
            } else h = 2 === d && b.has("tfl-eus") ? "tfl-lu" : "tfl-ang";
        b = Ga[h].B[fz];
        g[0] += Ga[h].C[fz];
        g[1] += b;
        c !== a && (e += "s");
        e += " " + fz
    }
    E && (g[1] = .05 * Math.round(.66 * g[1] / .05));
    a = r(f);
    for (c = a.next(); !c.done; c = a.next())
        if (f = r(c.value), c = f.next().value, f = f.next().value, "E" !== c && "T" != c || !(2 > f)) {
            if ("W" === c || "S" == c) {
                g = [0, 0];
                e = "special fares apply";
                break
            }
            f = Fa.get(c);
            g[0] += f[0];
            g[1] += f[1];
            e = 0 === d ? e + (" " + c) : e + ("+" + c)
        }
    return [e, g]
}

function bb(a) {
    a = Number(a).toString(16);
    2 > a.length && (a = "0" + a);
    return a
}
var cb = [];

function db(a, b) {
    if (0 === cb.length)
        for (var d = 0; 70 > d; d++) {
            var e = d,
                e = 70 - e,
                c, f;
            35 > e ? (c = 220, f = Math.round(e / 35 * 220)) : (f = 220, c = Math.round(220 * (1 - (e - 35) / 35)));
            cb.push("#" + bb(c) + bb(f) + "00")
        }
    if ("-" === a) return cb[0];
    if ("X" === a) return "#000000";
    a = Math.floor(a / b);
    return 70 > a ? cb[a] : "#cc5555"
}

function eb(a, b) {
    b = void 0 === b ? 1 : b;
    a = a.sort(function(a, b) {
        var c = r([a[1], b[1]]),
            d = c.next().value,
            c = c.next().value;
        return !(d < c || d > c)
    });
    O("#A8");
    var d = $("#A8")[0],
        e = r([d.width, d.height]),
        c = e.next().value,
        e = e.next().value;
    ctx = d.getContext("2d");
    for (var d = r(a), f = d.next(); !f.done; f = d.next()) {
        var f = r(f.value),
            g = f.next().value,
            h = f.next().value;
        0 === h && (h = "-");
        h === 1 / 0 && (h = "X");
        f = g.coords[0] * c;
        g = g.coords[1] * e;
        h = db(h, b);
        ctx.beginPath();
        ctx.fillStyle = h;
        ctx.strokeStyle = h;
        ctx.arc(f, g, 50, 0, 2 * Math.PI, !1);
        ctx.closePath();
        ctx.fill()
    }
}

function fb(a, b) {
    b = void 0 === b ? 1 : b;
    O("#A25");
    $("#A10").addClass(".B3".slice(1));
    var d = $("#A25")[0],
        e = r([d.width, d.height]),
        c = e.next().value,
        e = e.next().value,
        d = d.getContext("2d");
    d.L = !0;
    d.textAlign = "center";
    d.font = "bold 32px sans-serif";
    d.strokeStyle = "black";
    d.lineWidth = 5;
    for (var f = r(a), g = f.next(); !g.done; g = f.next()) {
        var g = r(g.value),
            h = g.next().value,
            g = g.next().value;
        0 === g ? g = "-" : g === 1 / 0 && (g = "X");
        var l = r(h.coords),
            h = l.next().value,
            l = l.next().value,
            l = r([h * c, l * e + 32 / 3]),
            h = l.next().value,
            l = l.next().value;
        d.fillStyle =
            db(g, b);
        d.strokeText(g, h, l);
        d.fillText(g, h, l)
    }
}

function gb(a) {
    var b = String(a.id);
    if (Ya.has(a)) return Ya.get(a);
    for (var d = new Map, b = Va.K(b, !1, !0), e = r(Object.keys(b)), c = e.next(); !c.done; c = e.next()) {
        c = c.value;
        time = b[c];
        var f = !1;
        c & 2097152 ? f = S[c - 2097152] : c & 4194304 && (f = T[c - 4194304]);
        d.set(f, time)
    }
    b = [];
    e = r(d);
    for (c = e.next(); !c.done; c = e.next())
        if (f = r(c.value), c = f.next().value, f = f.next().value, c instanceof L && !c.g && b.push([c, f]), c instanceof M) {
            for (var f = 1 / 0, g = r(c.l), h = g.next(); !h.done; h = g.next()) {
                var h = r(h.value),
                    l = h.next().value;
                h.next();
                f = Math.min(f,
                    d.get(l))
            }
            b.push([c, f])
        }
    Ya.set(a, b);
    return b
}

function hb(a) {
    var b = R[1];
    if (b === a) return !1;
    b = String(b.id);
    a = String(a.id);
    var d = Va.K(b, a);
    if (0 === d.length || d[0][0] !== a) return !1;
    b & 4194304 || (d = d.concat([
        [b, 0]
    ]));
    d.reverse();
    b = d.map(function(a) {
        var b;
        b = a[0];
        b = b & 2097152 ? S[b - 2097152] : b & 4194304 ? T[b - 4194304] : void 0;
        return [b, a[1]]
    });
    b[b.length - 1][0] instanceof M && b.pop();
    return b
}

function ib(a) {
    if (a = hb(a)) {
        var b = r(ab(a));
        fzone = b.next().value;
        fares = b.next().value;
        $("#A1c").html("" + a[a.length - 1][1]);
        $("#A1d").html("mins");
        $("#A1e").html("" + fzone);
        b = "\u00a3" + fares[0].toFixed(2) + "/\u00a3" + fares[1].toFixed(2);
        J && (b = "\u00a3" + fares[1].toFixed(2));
        E && (b += "*");
        "s" === fzone[0] && (b = "check tfl.gov.uk");
        $("#A1a").html(b);
        for (var b = [a[0]], d = 1; d < a.length - 1; d++) {
            var e = r(a[d]),
                c = e.next().value,
                e = e.next().value,
                f = a[d - 1][0].u !== c.u,
                g = a[d + 1][0].u !== c.u,
                h = r(a[d + 1]),
                l = h.next().value;
            h.next();
            (f ||
                g) && b.push([c, e]);
            !g || c instanceof M || l instanceof M || b.push([l.name.toLowerCase() + " (OSI)", e])
        }
        b.push(a[a.length - 1]);
        d = [];
        e = r(b[0]);
        c = e.next().value;
        e.next();
        d.push([c.i + " big", c.name.toLowerCase()]);
        for (e = 1; e < b.length - 1; e++) f = r(b[e]), c = f.next().value, f = f.next().value, "string" === typeof c ? d.push(["Interchange big", c]) : c instanceof L ? d.push([c.i, f + "m: " + c.name.toLowerCase()]) : d.push(["Interchange big", c.name.toLowerCase()]);
        b = r(b[b.length - 1]);
        c = b.next().value;
        b.next();
        d.push([c.i + " big", c.name.toLowerCase()]);
        b = $("#A19");
        b.empty();
        d = r(d);
        for (c = d.next(); !c.done; c = d.next()) e = r(c.value), c = e.next().value, e = e.next().value, c = $("<span>", {
            "class": c,
            text: e
        }), e = $("<br>"), b.append(c), b.append(e);
        jb(a)
    } else Oa(), $("#A1c").html("error"), $("#A1d").html("")
}

function kb(a) {
    $a() && (R[1] ? R[1] === a ? (Q(), V(a)) : (R[2] = a, lb(a, 2), ib(R[2])) : (R[1] = a, lb(a, 1)))
}

function V(a) {
    R[0] && Pa(R[0]);
    $("#" + a.id).addClass(".Bb".slice(1));
    var b = $("#Ad");
    $("#Ab").html("" + a.name.toLowerCase());
    var d = a.i;
    a instanceof M && (d = "Interchange");
    $("#Ab").removeClass().addClass(d);
    responsiveVoice.speak(a.name);
    var e = r(a.coords);
    x = e.next().value;
    y = e.next().value;
    e = ".B1d".slice(1);
    .5 > x && (e = ".B1c".slice(1));
    b.css({
        left: 100 * x + "%",
        top: 100 * y + "%"
    });
    b.removeClass().addClass(e).addClass(d);
    b.show();
    b = $("#Ac");
    b.css({
        left: 100 * x + "%",
        top: 100 * y + "%"
    });
    b.removeClass().addClass(d);
    b.show();
    R[0] = a;
    if (!R[2])
        if (!R[1]) {
            if (lb(a,
                    1, !0), A || C) a = gb(a), A && fb(a), C && eb(a)
        } else if (a === R[1] || R[2]) R[2] || ($("#A24").removeClass(), $("#A24").html("hover a station"), Oa());
    else if (lb(a, 2, !0), ib(a), B || D) {
        d = R[1];
        if (Za.has([a, d])) a = Za.get([a, d]);
        else {
            for (var c = gb(a), b = gb(d), b = new Map(b), e = [], c = r(c), f = c.next(); !f.done; f = c.next()) {
                var g = r(f.value),
                    f = g.next().value,
                    g = g.next().value;
                b.has(f) && e.push([f, g + b.get(f)])
            }
            Za.set([a, d], e);
            a = e
        }
        B && fb(a, 2);
        D && eb(a, 2)
    }
}

function Pa(a) {
    R[0] === a && (R[0] = !1);
    $("#Ad").hide();
    $("#Ac").hide();
    $("#" + a.id).removeClass(".Bb".slice(1))
}

function mb(a) {
    V(a);
    kb(a);
    Pa(a)
}

function lb(a, b, d) {
    d = void 0 === d ? !1 : d;
    var e = $("#A23");
    2 === b && (e = $("#A24"));
    var c = a.name.toLowerCase();
    2 === b && (c = "\u2192 " + c);
    e.html(c);
    a instanceof M ? e.removeClass().addClass("Interchange") : e.removeClass().addClass(a.i);
    d ? e.addClass(".B2".slice(1)) : e.removeClass(".B2".slice(1))
}

function nb(a) {
    var b = $("#A18")[0],
        d = r([b.width, b.height]),
        e = d.next().value,
        d = d.next().value,
        c = r(a.coords);
    a = c.next().value;
    c = c.next().value;
    d = r([a * e, c * d]);
    e = d.next().value;
    d = d.next().value;
    b = b.getContext("2d");
    b.beginPath();
    b.arc(e, d, 30, 2 * Math.PI, !1);
    b.fillStyle = "white";
    b.fill();
    b.lineWidth = 10;
    b.strokeStyle = "black";
    b.stroke()
}

function ob(a, b) {
    var d = $("#A18")[0],
        e = r([d.width, d.height]),
        c = e.next().value,
        f = e.next().value;
    a instanceof L && a.g && (a = a.g);
    b instanceof L && b.g && (b = b.g);
    var e = r(a.coords),
        g = e.next().value,
        h = e.next().value,
        l = r([g * c, h * f]),
        e = l.next().value,
        l = l.next().value,
        h = r(b.coords),
        g = h.next().value,
        h = h.next().value,
        f = r([g * c, h * f]),
        c = f.next().value,
        f = f.next().value,
        d = d.getContext("2d");
    d.lineWidth = 30;
    d.strokeStyle = "black";
    d.beginPath();
    d.moveTo(e, l);
    d.lineTo(c, f);
    d.stroke();
    d.lineWidth = 10;
    d.strokeStyle = "white";
    d.beginPath();
    d.moveTo(e, l);
    d.lineTo(c, f);
    d.stroke();
    nb(a);
    nb(b)
}

function jb(a) {
    if ($a()) {
        O("#A18");
        $("#A10").addClass(".B3".slice(1));
        a = a.map(function(a) {
            return a[0]
        });
        for (var b = [], d = new Set(a.filter(function(a) {
                return a instanceof L
            })); d.size;) {
            for (var e = r(d).next(); !e.done;) {
                var c = e.value;
                break
            }
            for (; c.prev[0] && d.has(c.prev[0]);) c = c.prev[0];
            b.push(c);
            d["delete"](c);
            for (e = c.next[0]; c.s && e && d.has(e);) c = c.s, c instanceof L && (e = c.next[0]), b.push(c), d["delete"](c)
        }
        pb(b, "route");
        pb(b, "routeoutline");
        for (c = 0; c < a.length; c++) b = a[c], c < a.length - 1 && b.u !== a[c + 1].u ? ob(b, a[c +
            1]) : (0 === c || c === a.length - 1 || b instanceof M) && nb(b)
    }
}
var qb = new Set(["DLR", "TFLRail", "Overground", "Emirates", "Tram"]);

function pb(a, b) {
    b = void 0 === b ? "normal" : b;
    var d = $("#A10")[0];
    if ("route" === b || "routeoutline" === b) d = $("#A18")[0];
    var e = r([d.width, d.height]),
        c = e.next().value,
        e = e.next().value,
        d = d.getContext("2d");
    "normal" === b ? d.lineWidth = 9 : "outline" === b ? (a = a.filter(function(a) {
        return qb.has(a.i)
    }), d.lineWidth = 3) : "route" === b ? d.lineWidth = 30 : (a = a.filter(function(a) {
        return qb.has(a.i)
    }), d.lineWidth = 10);
    for (var f = new Set(a); f.size;) {
        for (var g = r(f).next(); !g.done;) {
            var h = g.value;
            break
        }
        for (; h.A && f.has(h.A);) h = h.A;
        f["delete"](h);
        d.strokeStyle = "outline" === b || "routeoutline" == b ? "white" : Da.get(h.i);
        d.beginPath();
        var l = r(h.coords),
            g = l.next().value,
            l = l.next().value;
        for (d.moveTo(g * c, l * e); h.s && f.has(h.s);) h = h.s, f["delete"](h), l = r(h.coords), g = l.next().value, l = l.next().value, d.lineTo(g * c, l * e);
        d.stroke()
    }
}

function rb(a) {
    a.stopPropagation();
    if ($(this).hasClass(".B4".slice(1))) {
        $(".Bf").addClass(".B4".slice(1));
        $(this).removeClass(".B4".slice(1));
        a = $(this).data("map_toggle");
        var b = W();
        Ta();
        K = a;
        X();
        sb(b)
    }
}

function Qa() {
    var a = "#A1 #A16 #A1e #A1a #A23 #A24 #A1b".split(" ");
    if (J)
        for (var b = r(a), a = b.next(); !a.done; a = b.next()) $(a.value).addClass(".B14".slice(1));
    else
        for (b = r(a), a = b.next(); !a.done; a = b.next()) $(a.value).removeClass(".B14".slice(1))
}

function X() {
    if ($a()) {
        for (var a = r(xa()), b = a.next().value, a = a.next().value, d = za(b), e = new ra, c = r(b), f = c.next(); !f.done; f = c.next()) {
            for (var f = f.value, g = f.id, h = {}, l = r([f.prev, f.next]), m = l.next(); !m.done; m = l.next()) m = m.value, m[0] && (h[m[0].id] = m[1]);
            l = r(f.l);
            for (m = l.next(); !m.done; m = l.next()) {
                var p = r(m.value),
                    m = p.next().value,
                    p = p.next().value;
                h[m.id] = p
            }
            f.g && (h[f.g.id] = J && "North Acton" === f.name ? 0 : I);
            e.I(g, h)
        }
        c = r(d);
        for (f = c.next(); !f.done; f = c.next()) {
            h = f.value;
            f = h.id;
            g = {};
            h = r(h.l);
            for (l = h.next(); !l.done; l =
                h.next()) m = r(l.value), l = m.next().value, m = m.next().value, g[l.id] = m;
            e.I(f, g)
        }
        f = r(Aa(b));
        c = f.next().value;
        f = f.next().value;
        g = Ca(b, d);
        b = r([b, a, d, e, c, f, g]);
        S = b.next().value;
        Ua = b.next().value;
        T = b.next().value;
        Va = b.next().value;
        Wa = b.next().value;
        Xa = b.next().value;
        U = b.next().value;
        Qa();
        pb(S.concat(Ua));
        pb(S.concat(Ua), "outline");
        c = S;
        e = T;
        d = $("#A10")[0];
        a = r([d.width, d.height]);
        b = a.next().value;
        a = a.next().value;
        d = d.getContext("2d");
        c = r(c);
        for (f = c.next(); !f.done; f = c.next()) f = f.value, f.g || (h = r(f.coords), g = h.next().value,
            h = h.next().value, h = r([g * b, h * a]), g = h.next().value, h = h.next().value, d.beginPath(), d.arc(g, h, 10, 0, 2 * Math.PI, !1), d.fillStyle = Da.get(f.i), d.fill());
        e = r(e);
        for (c = e.next(); !c.done; c = e.next()) {
            c = c.value;
            d.fillStyle = "black";
            f = r(c.l);
            for (g = f.next(); !g.done; g = f.next()) g = r(g.value), h = g.next().value, g.next(), h = r(h.coords), g = h.next().value, h = h.next().value, h = r([g * b, h * a]), g = h.next().value, h = h.next().value, d.beginPath(), d.arc(g, h, 14, 0, 2 * Math.PI, !1), d.fill();
            d.fillStyle = "white";
            c = r(c.l);
            for (f = c.next(); !f.done; f = c.next()) f =
                r(f.value), g = f.next().value, f.next(), g = r(g.coords), f = g.next().value, g = g.next().value, g = r([f * b, g * a]), f = g.next().value, g = g.next().value, d.beginPath(), d.arc(f, g, 10, 0, 2 * Math.PI, !1), d.fill()
        }
    }
}

function W() {
    for (var a = [], b = r(R), d = b.next(); !d.done; d = b.next()) a.push(d.value);
    return a
}

function sb(a) {
    if ((a[0] || a[1] || a[2]) && !(R[0] || R[1] || R[2])) {
        var b = [];
        a = r(a);
        for (var d = a.next(); !d.done; d = a.next())
            if (d = d.value) {
                for (var e = !1, c = r(S), f = c.next(); !f.done; f = c.next())
                    if (f = f.value, f.name === d.name) {
                        f.g ? b.push(f.g) : b.push(f);
                        e = !0;
                        break
                    }
                if (!e) return
            } else b.push(!1);
        tb(b)
    }
}

function tb(a) {
    a[1] && mb(a[1]);
    a[2] && mb(a[2]);
    a[0] && V(a[0])
}

function ub(a) {
    a.stopPropagation();
    a = W();
    Ta();
    F ? (F = !1, $(this).addClass(".B4".slice(1))) : (F = !0, $(this).removeClass(".B4".slice(1)));
    X();
    sb(a)
}

function vb(a) {
    a.stopPropagation();
    a = W();
    Ta();
    J ? (J = !1, $(this).addClass(".B4".slice(1))) : (J = !0, $(this).removeClass(".B4".slice(1)));
    X();
    sb(a)
}

function wb(a) {
    a.stopPropagation();
    a = W();
    Ta();
    var b = $(this).data("time_toggle");
    $(".B1a").addClass(".B4".slice(1));
    $(this).removeClass(".B4".slice(1));
    I = b;
    X();
    sb(a)
}

function xb(a) {
    a.stopPropagation();
    a = W();
    Ta();
    var b = $(this).data("line_toggle");
    b === ".B19".slice(1) ? (H.clear(), $(".Bd").removeClass(".B4".slice(1))) : H.has(b) ? (H["delete"](b), $(this).removeClass(".B4".slice(1))) : (H.add(b), $(this).addClass(".B4".slice(1)));
    0 === H.size ? $(".Bd.B19").addClass(".B4".slice(1)) : $(".Bd.B19").removeClass(".B4".slice(1));
    X();
    sb(a)
}

function yb(a) {
    a.stopPropagation();
    a = W();
    Q();
    E ? (E = !1, $(this).addClass(".B4".slice(1))) : (E = !0, $(this).removeClass(".B4".slice(1)));
    tb(a)
}

function zb(a) {
    a.stopPropagation();
    a = W();
    Q();
    $(this).data("option_toggle")();
    $(this).toggleClass(".B4".slice(1));
    tb(a)
}
var S = [],
    Ua = [],
    T = [],
    Va = !1,
    Wa = new Map,
    Xa = new Map,
    U = [],
    R = [!1, !1, !1],
    Ya = new Map,
    Za = new Map;

function Ab() {
    var a = $("#A13"),
        b = new Set(S.map(function(a) {
            return a.i
        }));
    b.add(".B19".slice(1));
    var d = $("<div>", {
            id: "#Ae".slice(1),
            "class": ".B16".slice(1)
        }),
        e = $("<h3>", {
            "class": ".B17".slice(1),
            text: "lines"
        });
    d.append(e);
    for (var e = 0, b = r(b), c = b.next(); !c.done; c = b.next()) {
        c = c.value;
        if (0 === e || 8 === e) subsubcontainer_el = $("<div>", {
            id: "#Ae".slice(1) + "_" + (0 === e ? 1 : 2)
        }), d.append(subsubcontainer_el);
        var f = ".Bd".slice(1) + " " + ".B1b".slice(1),
            g = c.toLowerCase();
        "hammersmithcity" === g ? g = "h & city" : "waterloocity" ===
            g && (g = "w & city");
        c === ".B19".slice(1) && (g = "reset", f += " " + c + " " + ".B4".slice(1));
        f = $("<div>", {
            "class": f
        });
        f.data("line_toggle", c);
        f.click(xb);
        var h = $("<div>", {
            "class": ".Bc".slice(1) + " " + c
        });
        f.append(h);
        c = $("<span>", {
            text: g,
            "class": c
        });
        f.append(c);
        subsubcontainer_el.append(f);
        e++
    }
    a.append(d);
    d = $("<div>", {
        id: "#A12".slice(1),
        "class": ".B16".slice(1)
    });
    e = $("<h3>", {
        "class": ".B17".slice(1),
        text: "maps"
    });
    d.append(e);
    e = r(ta);
    for (b = e.next(); !b.done; b = e.next()) c = r(b.value), b = c.next().value, c = c.next().value, g =
        ".Bf".slice(1) + " " + ".B1b".slice(1), K != b && (g += " " + ".B4".slice(1)), g = $("<div>", {
            "class": g
        }), g.data("map_toggle", b), g.click(rb), b = $("<span>", {
            text: c
        }), g.append(b), d.append(g);
    a.append(d);
    a = $("#A14");
    d = $("<div>", {
        id: "#A17".slice(1),
        "class": ".B16".slice(1)
    });
    e = $("<h3>", {
        "class": ".B17".slice(1),
        text: "visuals"
    });
    d.append(e);
    e = r(sa);
    for (b = e.next(); !b.done; b = e.next()) g = r(b.value), b = g.next().value, c = g.next().value, g = g.next().value, f = ".B18".slice(1) + " " + ".B1b".slice(1), c || (f += " " + ".B4".slice(1)), c = $("<div>", {
        "class": f
    }), c.data("option_toggle", b), c.click(zb), b = $("<span>", {
        text: g
    }), c.append(b), d.append(c);
    a.append(d);
    d = $("<div>", {
        id: "#A2".slice(1),
        "class": ".B16".slice(1)
    });
    e = $("<h3>", {
        "class": ".B17".slice(1),
        text: "animations"
    });
    d.append(e);
    e = r(Bb);
    for (b = e.next(); !b.done; b = e.next()) c = r(b.value), b = c.next().value, c = c.next().value, g = ".B1".slice(1) + " " + ".B1b".slice(1), g += " " + ".B4".slice(1), g = $("<div>", {
        "class": g
    }), g.data("anim_toggle", b), g.click(Cb), b = $("<span>", {
        text: c
    }), g.append(b), d.append(g);
    a.append(d);
    d = $("<div>", {
        id: "#A7".slice(1),
        "class": ".B16".slice(1)
    });
    e = $("<h3>", {
        "class": ".B17".slice(1),
        text: "fares"
    });
    d.append(e);
    e = r([
        [ub, F, "toggle zone 1"],
        [vb, J, "night tube"],
        [yb, E, "railcard"]
    ]);
    for (b = e.next(); !b.done; b = e.next()) g = r(b.value), b = g.next().value, c = g.next().value, g = g.next().value, f = ".B6".slice(1) + " " + ".B1b".slice(1), c || (f += " " + ".B4".slice(1)), c = $("<div>", {
        "class": f
    }), c.click(b), b = $("<span>", {
        text: g
    }), c.append(b), d.append(c);
    a.append(d);
    d = $("<div>", {
        id: "#A27".slice(1),
        "class": ".B16".slice(1)
    });
    e = $("<h3>", {
        "class": ".B17".slice(1),
        text: "interchange"
    });
    d.append(e);
    subsubcontainer_el = $("<div>", {
        id: "#A26".slice(1)
    });
    e = r(ua);
    for (b = e.next(); !b.done; b = e.next()) b = b.value, c = ".B1a".slice(1) + " " + ".B1b".slice(1), b !== I && (c += " " + ".B4".slice(1)), c = $("<div>", {
        "class": c
    }), c.data("time_toggle", b), c.click(wb), g = b + "m", 0 === b && (g = "none"), b = $("<span>", {
        text: g
    }), c.append(b), subsubcontainer_el.append(c);
    d.append(subsubcontainer_el);
    a.append(d)
}

function Db(a, b) {
    if (a instanceof M) a: {
        for (var d = a.l, e = Math.floor(Math.random() * d.size), c = 0, d = r(d), f = d.next(); !f.done; f = d.next()) {
            var f = r(f.value),
                g = f.next().value;
            f.next();
            if (c === e) {
                a = g;
                break a
            }
            c++
        }
        a = void 0
    }
    e = b;
    c = a[e][0];
    c || (e = "next" === e ? "prev" : "next", c = a[e][0]);
    e = r([c, e]);
    tStation = e.next().value;
    b = e.next().value;
    return (e = tStation.g) ? [e, b] : [tStation, b]
}

function Eb() {
    var a = T.length,
        b = Math.floor(Math.random() * (a + S.length));
    if (b < a) return T[b];
    a = S[b - a];
    return a.g ? a.g : a
}
var Bb = [
        [Fb, "hover traverse"],
        [Gb, "single traverse"],
        [Hb, "dual traverse"]
    ],
    Y = ["next", "prev", "next"];

function Ib(a, b) {
    if (a) {
        if ("start" === a) {
            if ("h" === b) {
                R[0] || V(Eb());
                return
            }
            for (var d = r([R[1], R[2]]), e = d.next(); !e.done; e = d.next()) e.value || mb(Eb());
            return
        }
        if ("halt" === a) return
    }
    if ("h" === b) {
        if (d = R[0]) Q(), e = r(Db(d, Y[0])), d = e.next().value, Y[0] = e.next().value, V(d)
    } else d = W(), d[1] && d[2] && (Q(), "d" === b && (e = r(Db(d[1], Y[1])), d[1] = e.next().value, Y[1] = e.next().value), e = r(Db(d[2], Y[2])), d[2] = e.next().value, Y[2] = e.next().value, d[2] === d[1] && (e = r(Db(d[2], Y[2])), d[2] = e.next().value, Y[2] = e.next().value), tb(d))
}

function Hb(a) {
    Ib(void 0 === a ? !1 : a, "d")
}

function Gb(a) {
    Ib(void 0 === a ? !1 : a, "s")
}

function Fb(a) {
    Ib(void 0 === a ? !1 : a, "h")
}
var Ra = [!1, !1];

function Sa() {
    var a = Ra[0];
    a && (window.clearInterval(Ra[1]), Ra = [!1, !1], a("halt"))
}

function Cb(a) {
    a.stopPropagation();
    a = $(this).hasClass(".B4".slice(1));
    $(".B1").addClass(".B4".slice(1));
    a ? (a = $(this).data("anim_toggle"), Sa(), a("start"), Ra = [a, window.setInterval(a, 50)], $(this).removeClass(".B4".slice(1))) : Sa()
}

function Jb(a, b) {
    for (var d = b ? .05 : .02, e = $("#A28")[0], c = a.originalEvent, f = e.getBoundingClientRect(), g = r([f.right - f.left, f.bottom - f.top]), e = g.next().value, g = g.next().value, f = r([c.clientX - f.left, c.clientY - f.top]), c = f.next().value, f = f.next().value, g = r([c / e, f / g]), e = g.next().value, g = g.next().value, c = 0, f = U.length - 1, h = 0; c <= f;) {
        var l = Math.floor((c + f) / 2),
            m = U[l].coords[0] + d;
        if (m < e) c = l + 1;
        else if (m > e) f = l - 1;
        else {
            c = l;
            break
        }
        h++;
        if (20 <= h) return !1
    }
    if (c === U.length) return !1;
    f = [!1, 2 * d * d];
    for (h = 0; c + h < U.length;) {
        l = U[c + h];
        m = l.coords[0] - e;
        if (m > d) break;
        var p = l.coords[1] - g,
            m = m * m + p * p;
        m < f[1] && (f = [l, m]);
        h++;
        if (100 <= h) return !1
    }
    return f[0]
}

function Kb(a) {
    (a = Jb(a, !1)) && a !== R[0] && V(a)
}

function Lb(a) {
    a.stopPropagation();
    if (a = Jb(a, !0)) a !== R[0] && V(a), kb(a)
}
$(document).ready(function() {
    if ($a()) {
        $("#A28").on("mousemove", Kb);
        $("#A28").click(Lb);
        resetZoom = Mb();
        X();
        Ab();
        modes_div = $("#A16");
        modes_div.empty();
        for (var a = 0; a < Ia.length; a++) mode = Ia[a], $("#A9_" + mode).hide(), 0 === a && (mode_divider = $("<div>", {
                "class": ".B12".slice(1)
            }), modes_div.append(mode_divider)), mode_button = $("<div>", {
                "class": ".B11".slice(1),
                id: La(mode)
            }), mode_button.click(Na), mode_button.data("mode", mode), mode_button_text = $("<span>", {
                "class": ".B10".slice(1),
                text: mode
            }), mode_button.append(mode_button_text),
            modes_div.append(mode_button), mode_divider = $("<div>", {
                "class": ".B12".slice(1)
            }), modes_div.append(mode_divider);
        Ja("map", !0);
        $("#A1").fadeIn("fast")
    }
});

function Mb() {
    var a = $("#A28")[0],
        b = $(".B1e");

    function d() {
        f = c = 0;
        h = g = 1;
        t = p = m = l = 0;
        e()
    }

    function e() {
        w = 1 !== g ? "translate(" + c + "px, " + f + "px) scale(" + g + ")" : "";
        b.css({
            transform: w
        })
    }
    hammertime = new Hammer(a, {});
    hammertime.get("pinch").set({
        enable: !0
    });
    var c, f, g, h, l, m, p, t, w;
    d();
    hammertime.on("pan pinch panend pinchend", function(b) {
        1 !== g && (c = l + b.deltaX, f = m + b.deltaY, p = Math.ceil((g - 1) * a.clientWidth / 2), t = Math.ceil((g - 1) * a.clientHeight / 2), c > p && (c = p), c < -p && (c = -p), f > t && (f = t), f < -t && (f = -t));
        "pinch" === b.type && (g = Math.max(.999,
            Math.min(h * b.scale, 8)));
        "pinchend" === b.type && (h = g);
        "panend" === b.type && (l = c < p ? c : p, m = f < t ? f : t);
        e()
    });
    return d
};
var Z = [];

function Nb(a) {
    a = void 0 === a ? !1 : a;
    $("#A21").hide();
    for (var b = 0; 2 > b; b++) {
        var d = r([!1, !1]),
            e = d.next().value,
            d = d.next().value;
        b < Z.length && (d = r(Z[b]), e = d.next().value, d = d.next().value);
        var c = $("#A20_" + (b + 1));
        d ? (c.removeClass(), e ? 0 === b && $("#A21").show() : c.addClass(".B2".slice(1)), c.text(d.name.toLowerCase()), d instanceof M ? c.addClass("Interchange") : c.addClass(d.i), c.show()) : c.hide()
    }
    2 === Z.length && Z[1][0] && (Ka(), tb([!1, Z[0][1], Z[1][1]]), Z = [], Nb(), Ja("map"));
    a && (Ka(), mb(Z[0][1]), Z = [], Nb(), Ja("map"))
}

function Ob(a) {
    var b = $("#A1f"),
        d = b.val().toLowerCase();
    15 < d.length ? b.addClass(".Be".slice(1)) : b.removeClass(".Be".slice(1));
    b = !1;
    if ("keydown" === a.type)
        if ("Enter" === a.key) b = !0;
        else {
            "Delete" === a.key && ($("#A1f").val(""), Z = [], Nb());
            return
        }
    if ("click" === a.type) {
        a.stopPropagation();
        if (a.target === $("#A1f")[0]) return;
        $("#A1f").focus();
        b = !0
    }
    a: {
        a = b;a = void 0 === a ? !1 : a;
        if (0 < Z.length) {
            if (a && "" === d) {
                Nb(!0);
                break a
            }
            var b = r(Z[Z.length - 1]),
                e = b.next().value;
            b.next();
            e || Z.pop()
        }
        last4 = d.slice(-4);
        " to " === last4 && (d = d.slice(0, -4), a = !0);e = Ba(d);d = !1;
        if (Xa.has(e)) d = Xa.get(e);
        else {
            for (var b = [], e = r(e.split(" ")), c = e.next(); !c.done; c = e.next()) c = c.value, "" !== c && "to" !== c && (2 > c.length || (Wa.has(c) ? b.push(Wa.get(c)) : b.push(!1)));
            e = !1;
            b = r(b);
            for (c = b.next(); !c.done; c = b.next()) {
                c = c.value;
                if (!c) {
                    e = !1;
                    break
                }
                e || (e = new Set(c));
                for (var f = c, c = new Set, f = r(f), g = f.next(); !g.done; g = f.next()) g = g.value, e.has(g) && c.add(g);
                e = c
            }
            if (e)
                for (b = r(e), e = b.next(); !e.done; e = b.next())
                    if (d = e.value, 0 < Z.length && d === Z[0][1]) d = !1;
                    else break
        }
        d && (Z.push([a, d]), a &&
            $("#A1f").val(""));Nb()
    }
}
$(document).ready(function() {
    $("#A1f").on("input keydown", Ob);
    $("#A9_search").click(Ob);
    var a = $("#A4, #Af, #A22");
    $("#A1f").on("focus", function() {
        a.addClass(".B7".slice(1))
    });
    $("#A1f").on("blur", function() {
        a.removeClass(".B7".slice(1))
    })
});

function Pb() {
    var a = top.document,
        b = a.documentElement;
    a.fullscreenElement || a.mozFullScreenElement || a.webkitFullscreenElement || a.msFullscreenElement ? a.exitFullscreen ? a.exitFullscreen() : a.msExitFullscreen ? a.msExitFullscreen() : a.mozCancelFullScreen ? a.mozCancelFullScreen() : a.webkitExitFullscreen && a.webkitExitFullscreen() : b.requestFullscreen ? b.requestFullscreen() : b.msRequestFullscreen ? b.msRequestFullscreen() : b.mozRequestFullScreen ? b.mozRequestFullScreen() : b.webkitRequestFullscreen && b.webkitRequestFullscreen(Element.ALLOW_KEYBOARD_INPUT)
}
$(document).ready(function() {
    $("#Af").click(Pb)
});
